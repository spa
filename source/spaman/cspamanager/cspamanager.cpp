/*
 Simple Plugin Architecture
 aut: jv
 est: 061103

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#include "cspamanager.h"
#include <stdlib.h>
#include <list>
#include <vector>
using namespace std;

/**
 * SPA processor that does the core command processing
 */
template <bool SHALLOW>
class CSPAProcessor  {
private:
	CSPAManager *mMan;
	TSPACMD *mScript;
	bool mEndedOnReturn;
	std::list<char *> mToDelete;

public:
	CSPAProcessor(CSPAManager *man) : mMan(man), mEndedOnReturn(false) {}
	~CSPAProcessor()  { cleanup(); }

	/**
	 * Process & execute the script
	 * @param script The script to execute
	 * @warning May change the contents of script!
	 */
	char *process(TSPACMD *script)
	{
		cleanup();
		mEndedOnReturn = false;
		mScript = script;
		return processCommand(script);
	}

	bool endedOnReturn() const { return mEndedOnReturn; }

private:


	/*
	 * Cleanup the processor
	 */
	void cleanup()
	{
		for (std::list<char *>::iterator it = mToDelete.begin(); it != mToDelete.end(); ++it)
			mMan->delStr(&(*it));
		mToDelete.clear();
	}

	/**
	 * Execute the given command in the range [start, last], last->next should be semicolon or NULL
	 */
	char *execute(TSPACMD *start, TSPACMD *last)
	{
		if (!last->next)
			return mMan->call(start);
		else {
			TSPACMD *tmp = last->next;
			last->next = NULL;
			char *res = mMan->call(start);
			last->next = tmp;
			return res;
		}
	}

	/**
	 * End nested command - replaces the '(' token with the result, deletes the tokens between '(' and ')'
	 */
	void endNested(char *result, TSPACMD *start, TSPACMD *iter)
	{
		TSPACMD *first = start->next;

		// Replace
		if (SHALLOW)
			mToDelete.push_back(result);
		else
			mToDelete.push_back(start->token);
		start->token = result;

		if (iter)  {
			start->next = iter->next; // The token after ending round bracket
		
			// Unlink the processed part
			iter->next = NULL;
		} else  {
			start->next = NULL;
		}

		// Free
		if (SHALLOW)
			mMan->deleteShallowCopy(first);
		else
			mMan->delCmd(&first);		
	}

	/**
	 * Handle nested command
	 */
	void handleNested(TSPACMD *start)
	{
		TSPACMD *first = start->next;

		// Check that the opening bracket isn't last token
		if (!first)  {
			mMan->logError("CSPAManager::execute: Missing closing round bracket.");
			if (!SHALLOW)
				mToDelete.push_back(start->token);
			start->token = NULL;
			return;
		}

		TSPACMD *cmdStart = first;

		TSPACMD *iter = first;
		TSPACMD *prev = NULL;
		char *result = NULL;
		for (; iter; prev = iter, iter = iter->next)  {
			if (iter->token && iter->token[0] && iter->token[1] == 0)  {
				// End of command
				if (iter->token[0] == ';')  {
					mMan->delStr(&result);
					if (prev && cmdStart != iter)  {
						result = execute(cmdStart, prev);

						// Return - skip to the (correct) ending bracket
						if (handleReturn(result))  {
							unsigned int counter = 1;
							for (; counter && iter; prev = iter, iter = iter->next)  {
								if (iter->token && iter->token[1] == 0)  {
									if (iter->token[0] == '(')
										++counter;
									else if (iter->token[0] == ')')
										--counter;
								}
							}
							endNested(result, start, iter);
							return;
						}
					} else
						result = NULL;  // Empty command

					cmdStart = iter->next; // Move on
					continue;
				}
	
				// End of brackets
				if (iter->token[0] == ')')  {
					
					if (prev)  {
						if (cmdStart != iter)  {  // This happens when there's a semicolon before ending bracket, we do nothing in that case
							// A script inside brackets
							mMan->delStr(&result);
							result = execute(cmdStart, prev);
							handleReturn(result);
						}
					} else {
						// Empty brackets
						mMan->delStr(&result);
						result = NULL;
					}

					endNested(result, start, iter);
					return;
				}

				// Another nested bracket
				if (iter->token[0] == '(')
					handleNested(iter);
			}
		}

		// If we get here, the round brackets are not paired
		mMan->logError("CSPAManager::execute: Missing closing round bracket.");

		if (prev)  {
			if (cmdStart != iter)  {  // This happens when there's a semicolon before ending bracket, we do nothing in that case
				// A script inside brackets
				mMan->delStr(&result);
				result = execute(cmdStart, prev);
				handleReturn(result);
			}
		} else {
			// Empty command
			mMan->delStr(&result);
			result = NULL;
		}

		endNested(result, start, iter);
	}

	/**
	 * Process one command. The command can contain tokens or nested commands.
	 */
	char *processCommand(TSPACMD *cmd)
	{
		TSPACMD *cmdStart = cmd;
		TSPACMD *iter = cmd;
		TSPACMD *prev = NULL;
		char *result = NULL;

		// Iterate over the tokens
		for (; iter; prev = iter, iter = iter->next)
		{
			if (iter->token && iter->token[0] && iter->token[1] == 0)  {
				// End of command
				if (iter->token[0] == ';' )  {
					mMan->delStr(&result);
					if (prev && cmdStart != iter)  {
						result = execute(cmdStart, prev);
						if (handleReturn(result))
							return result;
					} else
						result = NULL; // Empty command
					cmdStart = iter->next;
					continue;
				}

				// Nested script
				if (iter->token[0] == '(')  {
					handleNested(iter);
				}
			}

			// End of command
			if (!iter->next)  {
				mMan->delStr(&result);
				result = execute(cmdStart, iter);
				handleReturn(result);
				break;
			}
		}

		return result;
	}

	/**
	 * Handle SPA return statement
	 * @return Returns true if the "return" statement occured and we should stop the processing
	 */
	bool handleReturn(char *& result)
	{
		// Handle return statement
		if (result && !strncmp(result, SPARETURN, SPARETURNLEN))  {		
			// Remove the return keyword (and possibly a whitespace following it) and return the rest

			char *tmpNewResult = result + SPARETURNLEN;

			// Skip space/tab following the return statement if necessary
			if (tmpNewResult[0] == ' ' || tmpNewResult[0] == '\t')
				++tmpNewResult;

			// Replace old result with new result
			char *newResult = mMan->newStr(tmpNewResult);
			mMan->delStr(&result);
			result = newResult;
			mEndedOnReturn = true;

			// Stop executing
			return true;
		}

		return false;
	}

};


//////////////////////////// SPA Manager implementation //////////////////////////////



SPACALL CSPAManager::CSPAManager() {
	mTokenizer = NULL;
	mCompFirst = NULL;
	mModuleLoader = NULL;
	mCompMap = new mTCOMPMAP;
	mCSComp = new CCrisec(CSSPINC);
	mMan = this;
	/*SPA commands registration*/
	SPACMDREG(moduleList, "Returns a list of component classes available from a module either printed in console (without \"silent\") or as command result (with \"silent\"). Usage: \"moduleList [silent] module\"");
	SPACMDREG(compClass, "Returns class of the given component. Usage: \"compClass component\"");
	SPACMDREG(compList, "Returns a space separated list of all components in reverse order of creation.");
	SPACMDREG(compModule, "Returns module of the given component. Usage: \"compModule component\"");
	SPACMDREG(compRefCount, "Returns number of references to the given component. Usage: \"compRefCount component\"");
//	SPACMDREG(halt, "Halts the process.");
	SPACMDREG_(delComponent, "delete", "Removes a component. Usage: \"delete name\"");
	SPACMDREG_(newComponent, "new", "Creates a new component from a module. Usage: \"new module class name\"");
	SPACMDREG(all, "For each component name (in reverse order of creation) executes \"name arguments\". Usage: \"all arguments\"");
	SPACMDREG(withAll, "For each component name (in reverse order of creation) executes \"argument1+\' \'+name+\' \'+argument2\". Usage: \"withAll argument1 argument2\"");
	SPACMDREG(withAllEx, "For each component name (in reverse order of creation) executes \"argument+\' \'+name+\' \'+module+\' \'+class+\' \'+referenceCount\". Usage: \"withAllEx argument\"");
}

SPACALL CSPAManager::~CSPAManager() {
	SDELETE(mCompMap);
	SDELETE(mCSComp);
}

/* IUnknown */

HRESULT STDMETHODCALLTYPE CSPAManager::QueryInterface(REFIID riid, void** ppvObj) {
	if (IsEqualGUID(riid, IID_ISPAManager)) {
		*ppvObj = static_cast<ISPAManager*>(this);
		AddRef();
		return S_OK;
	}
	if (IsEqualGUID(riid, IID_ISPACommonAccessControlLockable)) {
		*ppvObj = static_cast<ISPACommonAccessControlLockable*>(this);
		AddRef();
		return S_OK;
	}
	return CSPAComponent::QueryInterface(riid, ppvObj);
}

/* ISPAComponent */

char* STDMETHODCALLTYPE CSPAManager::call(TSPACMD* cmd) {
	if (mCompMap == NULL || mCmdMap == NULL) return NULL;
	char *funid = "CSPAManager::call: ";
	if (cmd == NULL || cmd->token == NULL) {
		logError((cstr(funid) + "NULL command.").c_str());
		return SERR;}
	mCSComp->enter();
	mTCOMPMAPITER compIter = mCompMap->find(cmd->token);
	if (compIter != mCompMap->end()) {
		mTCOMP *comp = compIter->second;
		++comp->refCount;
		mCSComp->leave();
		char * result = comp->component->call(cmd->next);
		mCSComp->enter();
		--comp->refCount;
		mCSComp->leave();
		return result;
	}
	mCSComp->leave();
	mTCMDMAPITER cmdIter = mCmdMap->find(cmd->token);
	if (cmdIter != mCmdMap->end()) return cmdIter->second->call(cmd->next);
	logError((cstr(funid) + "\"" + cmd->token + "\" is not a command nor a component. Use help.").c_str());
	return SERR;
}

/* ISPACommonAccessControlLockable */

void STDMETHODCALLTYPE CSPAManager::lock() {
	mCSComp->enter();
}

int STDMETHODCALLTYPE CSPAManager::tryLock() {
	return mCSComp->tryEnter();
}

void STDMETHODCALLTYPE CSPAManager::unlock() {
	mCSComp->leave();
}

/* ISPAManager */

ISPAComponent* STDMETHODCALLTYPE CSPAManager::getComponent(char* name) {
	mCSComp->enter();
	ISPAComponent *result;
	mTCOMPMAPITER iter = mCompMap->find(name);
	if (iter == mCompMap->end()) result = NULL;
	else {
		mTCOMP *aux = iter->second;
		aux->refCount++;
		result = aux->component;
		result->AddRef();
	}
	mCSComp->leave();
	return result;
}

HRESULT STDMETHODCALLTYPE CSPAManager::getComponentI(char* name, REFIID riid, void** ppvObj) {
#if (SPALOGLVL >= SPALOGLVL_DEBUG)
	char *funid = "CSPAManager::getComponent: ";
#endif
	if (!ppvObj) return E_INVALIDARG;
	mCSComp->enter();
	mTCOMPMAPITER iter = mCompMap->find(name);
	if (iter == mCompMap->end()) {
		*ppvObj = NULL;
		mCSComp->leave();
#if (SPALOGLVL >= SPALOGLVL_DEBUG)
		logError((cstr(funid) + "Component \"" + name + "\" not found.").c_str());
#endif
		return E_INVALIDARG;
	}
	else {
		mTCOMP *aux = iter->second;
		if (aux->component->QueryInterface(riid, ppvObj) == S_OK) {
			aux->refCount++;
			mCSComp->leave();
			return S_OK;
		}
		else {
			*ppvObj = NULL;
			mCSComp->leave();
#if (SPALOGLVL >= SPALOGLVL_DEBUG)
			logError((cstr(funid) + "Component \"" + name + "\" does not implement queried interface.").c_str());
#endif
			return E_NOINTERFACE;
		}
	}}

char* STDMETHODCALLTYPE CSPAManager::newStr(char* str) {
	if (str == NULL) return NULL;
	int l = 0;
	while(str[l++]);
	char *res = new char[l];
	memcpy(res, str, l*sizeof(char));
	return res;
}

void STDMETHODCALLTYPE CSPAManager::delStr(char** str) {
	if (*str != NULL) {delete[] *str; *str = NULL;}
}

TSPACMD* STDMETHODCALLTYPE CSPAManager::newToken(char* str) {
	TSPACMD *res = new TSPACMD;
	res->token = newStr(str);
	res->next = NULL;
	return res;
}

void STDMETHODCALLTYPE CSPAManager::delCmd(TSPACMD** cmd) {
	while (*cmd != NULL) {
		TSPACMD *aux=*cmd;
		*cmd = (*cmd)->next;
		delStr(&aux->token);
		delete aux;
	}
}

char* STDMETHODCALLTYPE CSPAManager::execute(char* cmd) {
	if (cmd == NULL) {logError("CSPAManager::execute: NULL in argument."); return SERR;}
	

	CSPAProcessor<false> processor(this);

	TSPACMD *script = NULL;
	mTokenizer->tokenize(cmd, &script);
	char *result = processor.process(script);
	delCmd(&script);
	return result;
}


char* STDMETHODCALLTYPE CSPAManager::executeCmd(TSPACMD* cmd) {
	if (cmd == NULL) {logError("CSPAManager::executeCmd: NULL in argument."); return SERR;}
	return process(cmd);
}

int STDMETHODCALLTYPE CSPAManager::releaseComponent(IUnknown** component) {
	char *funid = "CSPAManager::releaseComponent: ";
	if (component == NULL || *component == NULL) {
		logError((cstr(funid) + "NULL component.").c_str());
		return 0;
	}
	ISPAComponent *aux = NULL;
	if ((*component)->QueryInterface(IID_ISPAComponent, (void**)&aux) != S_OK || aux == NULL) {logError((cstr(funid) + "QueryInterface failed.").c_str()); return 0;}
	aux->Release();
	if (aux->name() == NULL) {logError((cstr(funid) + "NULL component name.").c_str()); return 0;}
	mCSComp->enter();
	mTCOMPMAPITER iter = mCompMap->find(aux->name());
	if (iter == mCompMap->end()) {logError((cstr(funid) + "Nonexistent component \"" + aux->name() + "\".").c_str()); mCSComp->leave(); return 0;}
	if (iter->second->refCount <= 0) {logError((cstr(funid) + "Component \"" + aux->name() + "\" not in use.").c_str()); mCSComp->leave(); return 0;}
	iter->second->refCount--;
	aux->Release();
	*component = NULL;
	mCSComp->leave();
	return 1;
}

TSPACMD* STDMETHODCALLTYPE CSPAManager::tokenize(char* cmd) {
	TSPACMD *res = NULL;
	mTokenizer->tokenize(cmd, &res);
	return res;
	// mcsComp->leave();
}

/*SPA commands definition*/
SPACMDDEF(CSPAManager, all) {
	char *funid = "CSPAManager::all: ";
	// if (cmd == NULL || cmd->token == NULL)
	//{ logError((cstr(funid) + "Missing argument.").c_str()); return SERR; }
	mCSComp->enter();
	if (mCompFirst == NULL) {
		mCSComp->leave();
		return NULL;
	}
	mTCOMP *aux = mCompFirst;
	list<cstr> lst;
	while (aux != NULL) {
		lst.push_back(aux->name);
		aux = aux->next;
	}
	mCSComp->leave();
	TSPACMD *auxCmd = NULL;
	char *result = NULL;
	for (list<cstr>::iterator i = lst.begin(); i != lst.end(); i++) {
		auxCmd = newToken((*i).c_str());
		auxCmd->next = cmd;
		delStr(&result);
		result = executeCmd(auxCmd);
		auxCmd->next = NULL;
		delCmd(&auxCmd);
	}
	return result;
}

SPACMDDEF(CSPAManager, compClass) {
	char *funid = "CSPAManager::compClass: ";
	if (cmd == NULL || cmd->token == NULL) {
		logError((cstr(funid) + "Missing argument.").c_str());
		return SERR;
	}
	mCSComp->enter();
	mTCOMPMAPITER iter = mCompMap->find(cmd->token);
	if (iter == mCompMap->end()) {
		logError((cstr(funid) + "Nonexistent component \"" + cmd->token + "\".").c_str());
		mCSComp->leave();
		return SERR;
	}
	mTCOMP *aux = iter->second;
	char* res = newStr(aux->componentName.c_str());
	mCSComp->leave();
	return res;
}

SPACMDDEF(CSPAManager, compList) {
	// char *funid = "CSPAManager::componentList: ";
	// if (cmd == NULL || cmd->token == NULL)
	//{ logError((cstr(funid) + "Missing argument.").c_str()); return SERR; }
	cstr lst;
	mCSComp->enter();
	mTCOMP *aux = mCompFirst;
	if (aux != NULL) {
		lst += aux->name;
		aux = aux->next;
	}
	while (aux != NULL) {
		lst += " ";
		lst += aux->name;
		aux = aux->next;
	}
	mCSComp->leave();
	return newStr(lst.c_str());
}

SPACMDDEF(CSPAManager, compModule) {
	char *funid = "CSPAManager::compModule: ";
	if (cmd == NULL || cmd->token == NULL) {
		logError((cstr(funid) + "Missing argument.").c_str());
		return SERR;
	}
	mCSComp->enter();
	mTCOMPMAPITER iter = mCompMap->find(cmd->token);
	if (iter == mCompMap->end()) {
		logError((cstr(funid) + "Nonexistent component \"" + cmd->token + "\".").c_str());
		mCSComp->leave();
		return SERR;
	}
	mTCOMP *aux = iter->second;
	char* res = newStr(aux->module->getModuleID());
	mCSComp->leave();
	return res;
}

SPACMDDEF(CSPAManager, compRefCount) {
	char *funid = "CSPAManager::compRefCount: ";
	if (cmd == NULL || cmd->token == NULL) {
		logError((cstr(funid) + "Missing argument.").c_str());
		return SERR;
	}
	mCSComp->enter();
	mTCOMPMAPITER iter = mCompMap->find(cmd->token);
	if (iter == mCompMap->end()) {
		logError((cstr(funid) + "Nonexistent component \"" + cmd->token + "\".").c_str());
		mCSComp->leave();
		return SERR;
	}
	mTCOMP *aux = iter->second;
	char* res = newStr(cstr(aux->refCount).c_str());
	mCSComp->leave();
	return res;
}

/*
 SPACMDDEF(CSPAManager, compList){
 // char *funid = "CSPAManager::componentList: ";
 // if (cmd == NULL || cmd->token == NULL)
 //{ logError((cstr(funid) + "Missing argument.").c_str()); return SERR; }
 cstr lst = "Components (in reverse order of creation): ";
 mcsComp->enter();
 mTCOMP *aux = mcompFirst;
 maddComponentListItem("name:", "class:", "references:", lst);
 while (aux != NULL){
 maddComponentListItem(aux->name, cstr(aux->module->getModuleID()) + ":" + aux->componentName, aux->refCount, lst);
 aux = aux->next;
 }
 mcsComp->leave();
 logInfo(lst.c_str());
 return SOK;
 }
 */

/*
 SPACMDDEF(CSPAManager, listColumnWidth){
 if (cmd != NULL&&cmd->token != NULL)
 mcomponentListColumnWidth = cstr(cmd->token).to_int();
 char *result = mMan->newStr(cstr(mcomponentListColumnWidth).c_str());
 return result;
 }
 */

SPACMDDEF(CSPAManager, delComponent) {
	char *funid = "CSPAManager::delete: ";
	if (cmd == NULL || cmd->token == NULL) {
		logError((cstr(funid) + "Missing argument.").c_str());
		return SERR;
	}
	mCSComp->enter();
	mTCOMPMAPITER iter = mCompMap->find(cmd->token);
	if (iter == mCompMap->end()) {
		logError((cstr(funid) + "Nonexistent component \"" + cmd->token + "\".").c_str());
		mCSComp->leave();
		return SERR;
	}
	mTCOMP *aux = iter->second;
	if (aux->refCount > 0) {
		logError((cstr(funid) + "Component \"" + cmd->token + "\" in use (" + aux->refCount + " reference(s)).").c_str());
		mCSComp->leave();
		return SERR;
	}

	mCompMap->erase(iter);
	if (aux->prev != NULL)
		aux->prev->next = aux->next;
	if (aux->next != NULL)
		aux->next->prev = aux->prev;
	if (aux == mCompFirst)
		mCompFirst = aux->next;

	aux->component->finish();

	// aux->component->Release();
	if (aux->component->Release() > 0) {
		logError((cstr("CSPAManager::delete: Unreleased component \"") + aux->component->name() + "\".").c_str());
		while (aux->component->Release() > 0)
			;
	}

	aux->module->release();
	mModuleLoader->deleteModule(aux->module);
	delStr(&aux->name);
	delete aux;

	mCSComp->leave();

	return SOK;
}
/*
SPACMDDEF(CSPAManager, halt) {
	exit(0);
	return NULL;
}
*/
SPACMDDEF(CSPAManager, moduleList) {
	char *funid = "CSPAManager::moduleList: ";
	int silent = 0;
	if (cmd != NULL && cstr("silent") == cmd->token) {
		silent = 1;
		cmd = cmd->next;
	}
	if (cmd == NULL || cmd->token == NULL) {
		logError((cstr(funid) + "Missing argument.").c_str());
		return SERR;
	}
	CModule *module;
	module = mModuleLoader->getModule(cmd->token);
	if (module == NULL) {
		logError((cstr(funid) + "Cannot load module \"" + cmd->token + "\".").c_str());
		return SERR;
	}
	cstr clist = module->getComponentList();
	module->release();
	mModuleLoader->deleteModule(module);
	if (clist == "") {
		logError((cstr(funid) + "Cannot get component list from module \"" + cmd->token + "\".").c_str());
		return SERR;
	}
	if (!silent) {
		logInfo((cstr(funid) + "Components available from module \"" + cmd->token + "\": " + clist).c_str());
		return SOK;
	} else
		return mMan->newStr(clist.c_str());
}

SPACMDDEF(CSPAManager, newComponent) {
	char *funid = "CSPAManager::new: ";
	char *invalid = "Invalid name \"";
	if (cmd == NULL || cmd->next == NULL || cmd->next->next == NULL || cmd->token == NULL || cmd->next->token == NULL
			|| cmd->next->next->token == NULL) {
		logError((cstr(funid) + "Missing argument(s).").c_str());
		return SERR;
	}
	if (!checkName(cmd->next->next->token)) {
		logError((cstr(funid) + invalid + cmd->next->next->token
				+ "\". Illegal character. Legal characters: 'A'-'Z', 'a'-'z', '0'-'9', '.', '_'.").c_str());
		return SERR;
	}
	if (checkExistsCommand(cmd->next->next->token)) {
		logError((cstr(funid) + invalid + cmd->next->next->token + "\". Name reserved.").c_str());
		return SERR;
	}
	if (checkExistsComponent(cmd->next->next->token)) {
		logError((cstr(funid) + invalid + cmd->next->next->token + "\". Component of this name already exists.").c_str());
		return SERR;
	}
	CModule *module;
	module = mModuleLoader->getModule(cmd->token);
	if (module == NULL) {
		logError((cstr(funid) + "Cannot load module \"" + cmd->token + "\".").c_str());
		return SERR;
	}
	ISPAComponent *comp;
	comp = module->getComponent(cmd->next->token, cmd->next->next->token);
	if (comp == NULL) {
		logError(
				(cstr(funid) + "Cannot get component \"" + cmd->next->token + "\" from module \"" + cmd->token + "\".").c_str());
		module->release();
		mModuleLoader->deleteModule(module);
		return SERR;
	}
	comp->AddRef();
	addComponent(newStr(cmd->next->next->token), cmd->next->token, comp, module);
	return SOK;
}

SPACMDDEF(CSPAManager, withAll) {
	char *funid = "CSPAManager::withAll: ";
	if (cmd == NULL || cmd->token == NULL) {
		logError((cstr(funid) + "Missing argument.").c_str());
		return SERR;
	}
	mCSComp->enter();
	if (mCompFirst == NULL) {
		mCSComp->leave();
		return NULL;
	}
	mTCOMP *aux = mCompFirst;
	list<cstr> lst;
	while (aux != NULL) {
		lst.push_back(aux->name);
		aux = aux->next;
	}
	mCSComp->leave();
	cstr arg1 = cmd->token, arg2;
	if (cmd->next != NULL)
		arg2 = cmd->next->token;
	char *result = NULL;
	for (list<cstr>::iterator i = lst.begin(); i != lst.end(); i++) {
		delStr(&result);
		result = execute((arg1 + " " + (*i) + " " + arg2).c_str());
	}
	return result;
}

SPACMDDEF(CSPAManager, withAllEx) {
	char *funid = "CSPAManager::withAllEx: ";
	if (cmd == NULL || cmd->token == NULL) {
		logError((cstr(funid) + "Missing argument.").c_str());
		return SERR;
	}
	mCSComp->enter();
	if (mCompFirst == NULL) {
		mCSComp->leave();
		return NULL;
	}
	mTCOMP *aux = mCompFirst;
	list<cstr> lst;
	while (aux != NULL) {
		lst.push_back(cstr(aux->name) + " " + aux->module->getModuleID() + " " + aux->componentName + " "
				+ aux->refCount);
		aux = aux->next;
	}
	mCSComp->leave();
	cstr arg = cmd->token;
	char *result = NULL;
	for (list<cstr>::iterator i = lst.begin(); i != lst.end(); i++) {
		delStr(&result);
		result = execute((arg + " " + (*i)).c_str());
	}
	return result;
}

#undef COMMAND

void SPACALL CSPAManager::addComponent(char *name, char* componentName, ISPAComponent* component, CModule* module) {
	mCSComp->enter();
	mTCOMP *aux = new mTCOMP(component, module, name, componentName, NULL, mCompFirst);
	if (mCompFirst != NULL) mCompFirst->prev = aux;
	mCompFirst = aux;
	mCompMap->insert(mTCOMPMAPVAL(name, aux));
	mCSComp->leave();
}

/*
 void SPACALL CSPAManager::maddComponentListItem(cstr name, cstr class, cstr refCount, cstr& list){
 list = list + "\r\n " + name;
 for (int i = 0; i<mcomponentListColumnWidth-name.length(); i++) list += " ";
 list += class;
 for (int i = 0; i<mcomponentListColumnWidth-class.length(); i++) list += " ";
 list += refCount;
 }
 */

int SPACALL CSPAManager::checkExistsCommand(char* name) {
	if (mCmdMap->find(name) == mCmdMap->end()) return 0;
	return 1;
}

int SPACALL CSPAManager::checkExistsComponent(char* name) {
	mCSComp->enter();
	if (mCompMap->find(name) == mCompMap->end()) {mCSComp->leave(); return 0;}
	mCSComp->leave();
	return 1;
}

int SPACALL CSPAManager::checkName(char* name) {
	// if (name == NULL) return 0;
	for (int i = 0; name[i]; i++)
	if (!((name[i]>='a'&&name[i]<='z') || (name[i]>='A'&&name[i]<='Z') || (name[i]>='0'&&name[i]<='9') || name[i]=='_'||name[i]=='.')) return 0;
	return 1;
}

TSPACMD* SPACALL CSPAManager::createShallowCopy(TSPACMD* cmd) {
	TSPACMD *result, *last;
	last = result = new TSPACMD;
	last->token = cmd->token;
	last->next = NULL;
	cmd = cmd->next;
	while (cmd != NULL) {
		TSPACMD *aux = new TSPACMD;
		aux->token = cmd->token;
		aux->next = NULL;
		last->next = aux;
		last = aux;
		cmd = cmd->next;
	}
	return result;
}

void SPACALL CSPAManager::deleteShallowCopy(TSPACMD* cmd) {
	while (cmd != NULL) {
		TSPACMD *aux = cmd;
		cmd = cmd->next;
		delete aux;
	}
}


char* SPACALL CSPAManager::process(TSPACMD* cmd) {
	CSPAProcessor<true> proc(this);
	TSPACMD *copy = createShallowCopy(cmd);
	char *res = proc.process(copy);
	deleteShallowCopy(copy);
	return res;
}


void SPACALL CSPAManager::destroy() {
	mCSComp->enter();
	mTCOMP *aux = mCompFirst;
	while (aux != NULL) {
		aux->component->finish();
		aux = aux->next;
	}
	finish();
	aux = mCompFirst;
	mCompMap->clear();
	while (aux != NULL) {
		//  aux->component->Release();
		if (aux->component->Release()>0) {
			logError((cstr("CSPAManager::destroy: Unreleased component \"") + aux->component->name() + "\".").c_str());
			while (aux->component->Release()>0);
		}
		//  logError((cstr("CSPAManager::destroy: Unreleased component \"") + aux->component->name() + "\" " + aux->component->Release()).c_str());
		aux->module->release();
		mModuleLoader->deleteModule(aux->module);
		aux->module = NULL;
		delStr(&aux->name);
		mTCOMP *tmp = aux;
		aux = aux->next;
		delete tmp;
	}
	mCSComp->leave();
}

/*
 void SPACALL CSPAManager::destroy(){
 typedef vector<ISPAComponent *> TSAVED;
 typedef TSAVED::iterator TSAVEDIT;
 TSAVED saved;
 mcsComp->enter();
 mTCOMP *aux = mcompFirst;
 while (aux != NULL){
 //  aux->component->finish();
 saved.push_back(aux->component);
 aux = aux->next;
 }
 mcsComp->leave();

 for (TSAVEDIT i = saved.begin(); i != saved.end(); i++)
 if (*i != NULL) (*i)->finish();
 saved.clear();

 mcsComp->enter();
 finish();
 aux = mcompFirst;
 mcompMap->clear();
 while (aux != NULL){
 //  aux->component->Release();
 if (aux->component->Release()>0){
 logError((cstr("CSPAManager::destroy: Unreleased component \"") + aux->component->name() + "\".").c_str());
 while (aux->component->Release()>0);
 }
 //  logError((cstr("CSPAManager::destroy: Unreleased component \"") + aux->component->name() + "\" " + aux->component->Release()).c_str());
 aux->module->release();
 mmoduleLoader->deleteModule(aux->module);
 aux->module = NULL;
 delStr(&aux->name);
 mTCOMP *tmp = aux;
 aux = aux->next;
 delete tmp;
 }
 mcsComp->leave();
 }
 */
