/*
 Simple Plugin Architecture
 aut: jv
 est: 061103

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef CSPAMANAGER_H
#define CSPAMANAGER_H

#include "..\..\..\include\spa\interfaces\ispamanager.h"
#include <spacommon\interfaces\accesscontrol\ispacommonaccesscontrollockable.h>
#include "ctokenizer.h"
#include "module.h"
#include "..\..\..\include\spa\tools\ccrisec.h"
#include "..\..\..\include\spa\tools\cpcharhashmap.h"
#include "..\..\..\include\spa\spaaux\cspaauxcomponent.h"
#include "..\..\..\include\spa\cspacomponent\cspacomponent.h"

#define SPARETURN "return"
#define SPARETURNLEN 6

class CSPAManager: public ISPAManager,
		public ISPACommonAccessControlLockable,
		virtual public CSPAComponent,
		virtual public CSPAAuxComponent {
protected:
	struct mTCOMP {
		ISPAComponent *component;
		CModule *module;
		char *name;
		cstr componentName;
		int refCount;
		mTCOMP *prev, *next;
		SPACALL mTCOMP(ISPAComponent* acomponent, CModule* amodule, char* aname, char* acomponentName, mTCOMP* aprev,
				mTCOMP* anext) {
			component = acomponent;
			module = amodule;
			name = aname;
			componentName = acomponentName;
			refCount = 0;
			prev = aprev;
			next = anext;
		}
	};
	typedef CPCharHashMap<mTCOMP*> mTCOMPMAP;
	typedef mTCOMPMAP::iterator mTCOMPMAPITER;
	typedef mTCOMPMAP::value_type mTCOMPMAPVAL;
	mTCOMPMAP *mCompMap;
	mTCOMP *mCompFirst;
	CCrisec *mCSComp;
	CModuleLoader *mModuleLoader;
	CTokenizer *mTokenizer;

	template <bool SHALLOW> friend class CSPAProcessor;

	inline void SPACALL addComponent(char* name, char* componentName, ISPAComponent* component, CModule* module);
	inline int SPACALL checkExistsCommand(char* name);
	inline int SPACALL checkExistsComponent(char* name);
	inline int SPACALL checkName(char* name);
	inline TSPACMD* SPACALL createShallowCopy(TSPACMD* cmd);
	inline void SPACALL deleteShallowCopy(TSPACMD* cmd);
	inline char* SPACALL process(TSPACMD* cmd);



	/*SPA commands*/
	SPACMDDECL(CSPAManager, all);
	SPACMDDECL(CSPAManager, compClass);
	SPACMDDECL(CSPAManager, compList);
	SPACMDDECL(CSPAManager, compModule);
	SPACMDDECL(CSPAManager, compRefCount);
	SPACMDDECL(CSPAManager, delComponent);
	SPACMDDECL(CSPAManager, moduleList);
	SPACMDDECL(CSPAManager, newComponent);
	SPACMDDECL(CSPAManager, withAll);
	SPACMDDECL(CSPAManager, withAllEx);

public:
	SPACALL CSPAManager();
	virtual SPACALL ~CSPAManager();
	virtual void SPACALL destroy();
	/* IUnknown */
	STDMETHOD(QueryInterface)(REFIID riid, void** ppvObj);
	STDMETHOD_(ULONG, AddRef)() {return CSPAComponent::AddRef();}
	STDMETHOD_(ULONG, Release)() {return CSPAComponent::Release();}
	/* ISPAComponent */
	STDMETHOD_(char*, call)(TSPACMD* cmd);
	/* ISPACommonAccessControlLockable */
	STDMETHOD_(void, lock)();
	STDMETHOD_(int, tryLock)();
	STDMETHOD_(void, unlock)();
	/* ISPAManager */
	STDMETHOD_(void, delStr)(char** str);
	STDMETHOD_(void, delCmd)(TSPACMD** cmd);
	STDMETHOD_(char*, execute)(char* cmd);
	STDMETHOD_(char*, executeCmd)(TSPACMD* cmd);
	STDMETHOD_(ISPAComponent*, getComponent)(char* name);
	STDMETHOD(getComponentI)(char* name, REFIID riid, void** ppvObj);
	STDMETHOD_(char*, newStr)(char* str);
	STDMETHOD_(TSPACMD*, newToken)(char* str);
	STDMETHOD_(int, releaseComponent)(IUnknown** component);
	STDMETHOD_(TSPACMD*, tokenize)(char* cmd);
};

#endif
