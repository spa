/*
 Simple Plugin Architecture
 aut: jv
 est: 060405

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef MODULE_H
#define MODULE_H

#include "..\..\..\include\spa\interfaces\ispacomponent.h"
#include "..\..\..\include\spa\base\spabase.h"

class CModule {
public:
	virtual SPACALL ~CModule() {
	}
	virtual ISPAComponent* SPACALL getComponent(char* componentID, char* name) = 0;
	virtual char* SPACALL getComponentList() = 0;
	virtual char* SPACALL getModuleID() = 0;
	virtual void SPACALL release() = 0;
};

class CModuleLoader {
public:
	virtual SPACALL ~CModuleLoader() {
	}
	virtual CModule* SPACALL getModule(char* moduleID) = 0;
	virtual void SPACALL deleteModule(CModule* module) = 0;
};

#endif
