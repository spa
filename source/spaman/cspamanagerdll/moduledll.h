/*
 Simple Plugin Architecture
 aut: jv
 est: 060412

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef MODULEDLL_H
#define MODULEDLL_H

#include <windows.h>
#include "..\cspamanager\module.h"
#include "..\..\..\include\spa\spaaux\cspaauxobject.h"
#include "..\..\..\include\spa\interfaces\ispamanager.h"
//#include "cspamanagerdll.h"
#include "..\..\..\include\spa\spadllfunctions.h"
#include "..\..\..\include\spa\tools\cstr.h"

typedef __SPADLL_GETCOMPONENT_TYPE (__SPADLL_CALL *TDLLGETCOMPONENT) __SPADLL_GETCOMPONENT_ATTR;
typedef __SPADLL_GETCOMPONENTLIST_TYPE (__SPADLL_CALL *TDLLGETCOMPONENTLIST) __SPADLL_GETCOMPONENTLIST_ATTR;
typedef __SPADLL_GETVERSION_TYPE (__SPADLL_CALL *TDLLGETVERSION) __SPADLL_GETVERSION_ATTR;
typedef __SPADLL_RELEASE_TYPE (__SPADLL_CALL *TDLLRELEASE) __SPADLL_RELEASE_ATTR;

class CModuleDll: public CModule, public CSPAAuxObject {
protected:
	HINSTANCE mDll;
	TDLLGETCOMPONENT mGetComponent;
	TDLLRELEASE mRelease;
	ISPAManager* mMan;
	cstr mModuleID;
public:
	SPACALL CModuleDll() {
		mDll = NULL;
		mGetComponent = NULL;
		mRelease = NULL;
		mMan = NULL;
		mOut = NULL;
	}
	SPACALL CModuleDll(cstr moduleID, ISPAManager* man, CSPAAuxOut* out, HINSTANCE dll, TDLLGETCOMPONENT getComponent,
			TDLLRELEASE release) :
		CSPAAuxObject(out) {
		mModuleID = moduleID;
		mMan = man;
		mDll = dll;
		mGetComponent = getComponent;
		mRelease = release;
	}
	virtual SPACALL ~CModuleDll() {
	}
	virtual ISPAComponent* SPACALL getComponent(char* componentID, char* name);
	virtual char* SPACALL getComponentList();
	virtual char* SPACALL getModuleID() {if (mMan != NULL) return mModuleID.c_str(); else return NULL;}
	virtual void SPACALL release();
};

class CModuleLoaderDll: public CModuleLoader, public CSPAAuxObject {
protected:
	ISPAManager* mMan;
public:
	SPACALL CModuleLoaderDll() {
		mMan = NULL;
		mOut = NULL;
	}
	SPACALL CModuleLoaderDll(ISPAManager* man, CSPAAuxOut* out) :
		CSPAAuxObject(out) {
		mMan = man;
	}
	virtual SPACALL ~CModuleLoaderDll() {
	}
	virtual CModule* SPACALL getModule(char* moduleID);
	virtual void SPACALL deleteModule(CModule* module) {delete module;}
};

#endif
