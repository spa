/*
 Simple Plugin Architecture
 aut: jv
 est: 060405

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef CSPAMANAGERDLL_H
#define CSPAMANAGERDLL_H

#include "..\cspamanager\cspamanager.h"

class CSPAManagerDll: virtual public CSPAManager {
protected:
public:
	SPACALL CSPAManagerDll();
	virtual SPACALL ~CSPAManagerDll();
};

#endif
