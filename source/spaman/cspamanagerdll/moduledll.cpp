/*
 Simple Plugin Architecture
 aut: jv
 est: 060405

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#include "moduledll.h"
#include "..\..\..\include\spa\base\spabase.h"

ISPAComponent* SPACALL CModuleDll::getComponent(char* componentID, char *name) {
	if (mDll == NULL || mGetComponent == NULL || mRelease == NULL/*||mMan == NULL*/) return NULL;
	ISPAComponent* component = NULL;
	__SPADLL_GETCOMPONENT_TYPE result = mGetComponent(&component, componentID, name, mMan);
	char *funid = "CModuleDll::getComponent: ";
	// cstr hlpmsg = cstr(" (dll: \"") + mmoduleID + "\", component: \"" + componentID + "\", name: \"" + name + "\").";
	if (result == GETCOMPONENT_RESULT_OK) return component; else
	if (result == GETCOMPONENT_RESULT_NOTFOUND) {
		cstr hlpmsg = cstr(" (dll: \"") + mModuleID + "\", component: \"" + componentID + "\", name: \"" + name + "\").";
		logError((cstr(funid) + "Component not found" + hlpmsg).c_str());
	} else
	if (result == GETCOMPONENT_RESULT_PARAMERR) {
		cstr hlpmsg = cstr(" (dll: \"") + mModuleID + "\", component: \"" + componentID + "\", name: \"" + name + "\").";
		logError((cstr(funid) + "Parametr error" + hlpmsg).c_str());
	} else
	if (result == GETCOMPONENT_RESULT_INTERNALERR) {
		cstr hlpmsg = cstr(" (dll: \"") + mModuleID + "\", component: \"" + componentID + "\", name: \"" + name + "\").";
		logError((cstr(funid) + "Dll internal error" + hlpmsg).c_str());
	} else {
		cstr hlpmsg = cstr(" (dll: \"") + mModuleID + "\", component: \"" + componentID + "\", name: \"" + name + "\").";
		logError((cstr(funid) + "Unknown dll error code" + hlpmsg).c_str());
	}
	return NULL;
}

char* SPACALL CModuleDll::getComponentList() {
	char *funid = "CModuleDll::getComponentList: ";
	TDLLGETCOMPONENTLIST getComponentList = (TDLLGETCOMPONENTLIST)GetProcAddress(mDll, __TOSTR(__SPADLL_GETCOMPONENTLIST_NAME));
	if (getComponentList == NULL) {
		cstr hmgpa = cstr(funid) + "GetProcAddress(\"", hmfailed = cstr("\") failed on library \"") + mModuleID + "\".";
		logError((hmgpa + __TOSTR(__SPADLL_GETCOMPONENTLIST_NAME) + hmfailed).c_str());
		return NULL;
	}
	char *result = getComponentList();
	return result;
}

void SPACALL CModuleDll::release() {
	if (mDll == NULL || mGetComponent == NULL || mRelease == NULL/*||mMan == NULL*/) return;
	mRelease();
	FreeLibrary(mDll);
}

//==============================================================================
//==============================================================================
//==============================================================================


CModule* SPACALL CModuleLoaderDll::getModule(char* moduleID) {
	char *funid = "CModuleLoaderDll::getModule: ";
	HINSTANCE dll = LoadLibrary(moduleID);
	if (dll == NULL) {
		logError((cstr(funid) + "LoadLibrary(\"" + moduleID + "\") failed.").c_str());
		return NULL;
	}
	// cstr hmgpa = cstr(funid) + "GetProcAddress(\"", hmfailed = cstr("\") failed on library \"") + moduleID + "\".";
	TDLLGETVERSION version = (TDLLGETVERSION)GetProcAddress(dll, __TOSTR(__SPADLL_GETVERSION_NAME));
	if (version == NULL) {
		cstr hmgpa = cstr(funid) + "GetProcAddress(\"", hmfailed = cstr("\") failed on library \"") + moduleID + "\".";
		logError((hmgpa + __TOSTR(__SPADLL_GETVERSION_NAME) + hmfailed).c_str());
		FreeLibrary(dll);
		return NULL;
	}
	if (!SPAVERSIONCORRECT(version())) {
		cstr hmgpa = cstr(funid) + "GetProcAddress(\"", hmfailed = cstr("\") failed on library \"") + moduleID + "\".";
		logError((cstr(funid) + "Incorrect version \"" + version() + "\" of library \"" + moduleID + "\".").c_str());
		FreeLibrary(dll);
		return NULL;
	}
	TDLLGETCOMPONENT getComponent = (TDLLGETCOMPONENT)GetProcAddress(dll, __TOSTR(__SPADLL_GETCOMPONENT_NAME));
	if (getComponent == NULL) {
		cstr hmgpa = cstr(funid) + "GetProcAddress(\"", hmfailed = cstr("\") failed on library \"") + moduleID + "\".";
		logError((hmgpa + __TOSTR(__SPADLL_GETCOMPONENT_NAME) + hmfailed).c_str());
		FreeLibrary(dll);
		return NULL;
	}
	TDLLRELEASE release = (TDLLRELEASE)GetProcAddress(dll, __TOSTR(__SPADLL_RELEASE_NAME));
	if (release == NULL) {
		cstr hmgpa = cstr(funid) + "GetProcAddress(\"", hmfailed = cstr("\") failed on library \"") + moduleID + "\".";
		logError((hmgpa + __TOSTR(__SPADLL_RELEASE_NAME) + hmfailed).c_str());
		FreeLibrary(dll);
		return NULL;
	}
	return new CModuleDll(moduleID, mMan, mOut, dll, getComponent, release);
}

