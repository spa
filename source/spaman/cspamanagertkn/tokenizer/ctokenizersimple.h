#ifndef CTOKENIZERSIMPLE_H
#define CTOKENIZERSIMPLE_H

#include "../../../../include/spa/interfaces/ispamanager.h"
#include "../../cspamanager/ctokenizer.h"
#include "../../../../include/spa/spaaux/cspaauxobject.h"

// WARNING: this class is NOT threadsafe
class CTokenizerSimple: public CTokenizer, public CSPAAuxObject {
protected:
	friend class CSPArser;

	ISPAManager *mMan;

	void SPACALL destroy(void);
	void SPACALL addToken(char *token, size_t size, TSPACMD **last, TSPACMD **result);
public:
	SPACALL CTokenizerSimple(ISPAManager *man, CSPAAuxOut* out): CSPAAuxObject(out) { mMan = man; }
	virtual SPACALL ~CTokenizerSimple(void) {destroy();}
	virtual int SPACALL tokenize(char* str, TSPACMD **res);
};

#endif
