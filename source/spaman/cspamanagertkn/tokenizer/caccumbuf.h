#ifndef CACCUMBUF_H
#define CACCUMBUF_H

// Accumulation string buffer, optimized for a fast adding of single characters
// and string chunks
#undef max
#include <algorithm>
#include <math.h>

template <typename T>
class CAccumBuf  {
private:
	static const size_t MAX_MEM = 0x80000000;
	static const size_t MIN_MEM = 0x80;
	T *mBuf;
	size_t mLength, mAllocated;

private:
	void checkRealloc(size_t appLen)
	{
		if (mLength + appLen < mAllocated)
			return;

		//mAllocated = std::max(mLength + appLen + MIN_MEM, (size_t)(1.0/(1.0 + exp(-(double)MAX_MEM))));
		size_t olda = mAllocated;
		mAllocated += mAllocated / 2;

		
		T* oldbuf = mBuf;
		mBuf = new T[mAllocated];
		memcpy(mBuf, oldbuf, olda * sizeof(T));
		delete[] oldbuf;
	}

public:
	CAccumBuf()  {
		mBuf = new T[MIN_MEM];
		mBuf[0] = (T)0;
		mLength = 0;
		mAllocated = MIN_MEM;
	}

	~CAccumBuf()  {
		delete[] mBuf;
		mLength = 0;
		mAllocated = 0;
		mBuf = NULL;
	}

	size_t length() const { return mLength; }
	size_t size() const { return mLength; }
	void clear() { mLength = 0; mBuf[0] = (T)0; }

	T operator[] (size_t index) const  { return mBuf[index]; }
	CAccumBuf& operator+= (T elem)  { checkRealloc(1); mBuf[mLength] = elem; mBuf[++mLength] = (T)0; return *this; }
	void append(T *start, size_t len)  { checkRealloc(len); memcpy(mBuf + mLength, start, len * sizeof(T)); mLength += len; mBuf[mLength] = (T)0; }
	void assign(T *str, size_t len)  { mLength = 0; checkRealloc(len); memcpy(mBuf, len * sizeof(T)); mLength = len; mBuf[len] = (T)0; }
	CAccumBuf& operator= (T elem)  { mBuf[0] = elem; mBuf[1] = (T)0; mLength = 1; return *this; }

	T *c_str() { return mBuf; }

};


#endif // CACCUMBUF_H
