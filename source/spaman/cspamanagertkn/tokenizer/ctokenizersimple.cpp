#include <list>
#include <string>
#include "ctokenizersimple.h"
#include "../../../../include/spa/tools/cstr.h"
#include "../../../../include/spa/spaaux/spaarg.h"
#include "caccumbuf.h"

/**
 * Helper function for the parser, returns true if the given character is a whitespace
 * @param c The character to check
 * @return True if c is whitespace
 */
inline bool is_space(char c)  
{
	return c == ' ' || c == '\n' || c == '\r' || c == '\t';
}

/**
 * Finite state machine for parsing the SPA string (push-down automaton)
 */
class CSPArser  {
private:
	char *input;
	size_t pos;
	CTokenizerSimple *tokenizer;
	TSPACMD **result;
	TSPACMD *last;
	CAccumBuf<char> currentToken;
	std::list<cstr> errors;

public:
	/**
	 * Constructor.
	 * @param t Pointer to the tokenizer owning this parser
	 */
	CSPArser(CTokenizerSimple *t) : tokenizer(t) {}

	/**
	 * Parses the input and returns the parsed token list.
	 * @param inp The input string to parse
	 * //@param breakParser A variable that can be used for breaking the parser (if it's running in a separate thread for example)
	 */
	void parse(char *inp, TSPACMD **res)  
	{
		currentToken.clear();
		result = res;
		last = NULL;
		errors.clear();
		input = inp;
		pos = 0;
		status_programStart();
	}

	/**
	 * Get list of parse errors
	 * @return Parse errors or empty list if the input string was valid
	 */
	std::list<cstr>& getErrors() { return errors; }

private:
	void endToken()
	{
		if (currentToken.length() != 0)  {
			tokenizer->addToken(currentToken.c_str(), currentToken.size(), &last, result);
			currentToken.clear();
		}
	}

	void addSpecialToken(char c)
	{
		tokenizer->addToken(&c, 1, &last, result);
	}

	void recoverAfterBreak()
	{
		tokenizer->mMan->delCmd(result);
		*result = NULL;
	}

	void status_programStart()
	{
		// Parse all commands
		while (input[pos])
			status_command();
	}

	void status_commentMulti()
	{
		for (; input[pos]; ++pos)  {
			// End of comment
			if (input[pos] == '*' && input[pos + 1] == '/')  {
				pos += 2;
				return;
			}
		}

		errors.push_back(cstr("Unexpected end of string found when tokenizing a multiline comment."));
	}

	void status_commentSingle()
	{
		for (; input[pos]; ++pos)  {
			if (input[pos] == '\n')  {
				++pos;
				return;
			}
		}
	}

	void status_command()
	{
		while (true)  {
			switch (input[pos])  {

			// Program end
			case '\0':
				endToken();
				return;

			// Command separator
			case ';':  {
				endToken();
				addSpecialToken(';');
				++pos;
				return;
			} break;

			// Spaces
			case ' ':
			case '\r':
			case '\n':
			case '\t':
			{
				endToken();
				++pos; // Skip
				continue;
			} break;

			// Comment
			case '/':  {
				if (input[pos + 1] == '*')  {
					endToken();
					pos += 2;
					status_commentMulti();
					continue;
				} else if (input[pos + 1] == '/')  {
					endToken();
					pos += 2;
					status_commentSingle();
					continue;
				}

				currentToken += '/';
				++pos;
				continue;
			} break;

			// { and }
			case '{':  {
				endToken();
				++pos;
				status_curly();
				continue;
			} break;

			// ( and )
			case '(':  {
				endToken();
				++pos;
				status_round();
				continue;
			} break;

			// " and "
			case '\"':  {
				endToken();
				++pos;
				status_stringQuot(true);
				continue;
			} break;

			// ' and ' (supports escape)
			case '\'':  {
				endToken();
				++pos;
				status_stringApos();
				continue;
			} break;

			default:  {
				// A simple character
				currentToken += input[pos];
				++pos;
			} break;
			}	  
		}
	}


	void status_curly()
	{
		status_nestedCurly();
		endToken();
	}

	void SPACALL status_nestedCurly()
	{
		// HINT: if there are more nested curly brackets, these are not called recursivelly but
		// a counter is incremented to avoid a recursive call
		size_t counter = 1;

		// HINT: the opening bracket has already been skipped
		while (true)  {

			switch (input[pos])  {
			// End of program
			case '\0':
				errors.push_back(cstr("Unexpected end of string found when tokenizing a curly bracket command."));
				return;

			// Ending bracket
			case '}':  {
				++pos;
				--counter;
				if (!counter)
					return;
				currentToken += '}';
				continue;
			} break;

			case '{':  { // Curly
				++pos;
				currentToken += '{';
				++counter;
				continue;
			} break;

			// String
			case '"':  {
				++pos;
				currentToken += '"';
				status_stringQuot(false);
				currentToken += '"';
				continue;
			} break;

			// String with escape 
			case '\'':  {
				++pos;
				currentToken += '\'';
				status_stringAposNested();
				currentToken += '\'';
				continue;
			} break;

			// Comment
			case '/':  {
				if (input[pos + 1] == '*')  {
					pos += 2;
					status_commentMulti();
					continue;
				} else if (input[pos + 1] == '/')  {
					pos += 2;
					status_commentSingle();
					continue;
				}
				currentToken += '/';
				++pos;
				continue;
			} break;

			// Any other character
			default: {
				currentToken += input[pos];
				++pos;
			} break;
			}
		}
	}

	void SPACALL status_round()
	{
		// Indicates the level of nested brackets
		size_t counter = 1;

		addSpecialToken('(');

		// HINT: the opening bracket has already been skipped
		while (true)  {
			switch (input[pos])  {
			// End of program
			case '\0':
				// Terminate all non-terminated brackets
				for (; counter; --counter)
					addSpecialToken(')');

				// This is not really nice, report it...
				errors.push_back(cstr("Unexpected end of string found when tokenizing a round bracket command."));
			return;

			// Command separator
			case ';':  {
				endToken();
				addSpecialToken(';');
				++pos;
				continue;
			} break;

			// Spaces
			case ' ':
			case '\r':
			case '\n':
			case '\t':
			{
				endToken();
				++pos; // Skip
				continue;
			} break;

			// Ending bracket
			case ')':  {
				++pos;
				endToken();
				addSpecialToken(')');
				--counter;
				if (!counter)
					return;
				continue;
			} break;

			case '{':  {  // Curly
				endToken();
				++pos;
				status_nestedCurly();
				endToken();
				continue;
			} break;

			case '(':  { // Round
				endToken();
				++pos;
				++counter;
				addSpecialToken('(');
				continue;
			} break;

			// String
			case '"':  {
				endToken();
				++pos;
				status_stringQuot(true);
				continue;
			} break;

			// String with escape 
			case '\'':  {
				endToken();
				++pos;
				status_stringApos();
				continue;
			} break;

			// Comment
			case '/':  {
				endToken();
				if (input[pos + 1] == '*')  {
					pos += 2;
					status_commentMulti();
					continue;
				} else if (input[pos + 1] == '/')  {
					pos += 2;
					status_commentSingle();
					continue;
				}
				currentToken += '/';
				++pos;
				continue;
			} break;

			// Any other character
			default: {
				currentToken += input[pos];
				++pos;
			} break;
			}
		}
	}

	void status_stringQuot(bool shouldEndToken)
	{
		// HINT: opening quote already skipped
		// HINT: quotes don't support escape sequences

		for (; input[pos]; ++pos)  {

			// String end
			if (input[pos] == '"')  {
				++pos;
				if (shouldEndToken)
					endToken();
				return;
			}

			currentToken += input[pos];
		}

		errors.push_back(cstr("Unexpected end of string found when tokenizing a string literal in quotation marks."));		
	}

	void status_stringAposNested()
	{
		// HINT: opening quote already skipped
		// HINT: escape sequences are not processed in nested strings (as they will be reparsed)

		for (; input[pos]; ++pos)  {

			// String end
			if (input[pos] == '\'')  {
				++pos;
				return;
			}

			currentToken += input[pos];
		}

		errors.push_back(cstr("Unexpected end of string found when tokenizing a string literal in apostrophes."));			
	}

	void status_stringApos()
	{
		// HINT: opening quote already skipped

		bool wasEscapeSymbol = false;
		for (; input[pos]; ++pos)  {

			// Handle escape sequence
			if (wasEscapeSymbol)  {
				if (input[pos] == '\'' || input[pos] == '\\')
					currentToken += input[pos];
				else  {
					// Unknown sequence, just add the backslash as well
					currentToken += '\\';
					currentToken += input[pos];
				}
				wasEscapeSymbol = false;
				continue;
			}

			// String end
			if (input[pos] == '\'')  {
				++pos;
				endToken();
				return;
			}

			if (input[pos] == '\\')  {
				wasEscapeSymbol = true;
				continue;
			}

			currentToken += input[pos];
		}

		errors.push_back(cstr("Unexpected end of string found when tokenizing a string literal in apostrophes."));
	}

};

/////////////////// Tokenizer implementation ///////////////////////


void SPACALL CTokenizerSimple::destroy(void) {

}

void SPACALL CTokenizerSimple::addToken(char *token, size_t size, TSPACMD **last, TSPACMD **result)
{
	// Create the token
	TSPACMD *tkn = mMan->newToken(NULL);
	tkn->token = new char[size + 1];
	memcpy(tkn->token, token, size);
	tkn->token[size] = 0;
	tkn->next = NULL;

	if (!*last)  { // First token
		*last = tkn;
		*result = tkn;
	} else {  // Any other token
		(*last)->next = tkn;
		*last = tkn;
	}
}

/**
 * Single-threaded tokenizing
 */
int SPACALL CTokenizerSimple::tokenize(char* str, TSPACMD **res) 
{
	FUNID(CTokenizerSimple, tokenize);
	// Parse
	CSPArser sparser(this);
	sparser.parse(str, res);
	if (!sparser.getErrors().empty()) {
		cstr scriptStart;
		for (int i = 0; i < 50 && str[i]; i++) scriptStart += str[i];
		if (scriptStart.length() >= 50 && str[49] && str[50]) scriptStart += "...";
		for (std::list<cstr>::iterator i = sparser.getErrors().begin(); i != sparser.getErrors().end(); i++)
			logWarn(FUNID_MSG("Scanner error \"" + *i + "\" in script \"" + str + "\"."));
	}
	return 1;
}

