/*
 Simple Plugin Architecture
 aut: jv
 est: 060609

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#include <windows.h>

#include "cspaconsolemb.h"
#include "..\..\..\include\spa\tools\cstr.h"

int STDMETHODCALLTYPE CSPAConsoleMB::message(char* name, char* msg, int type, int flags) {
	char *caption = NULL;
	int mbType;
	switch (type) {
		case SPACONMSG_INFO: caption = "Info"; mbType = MB_ICONINFORMATION; break;
		case SPACONMSG_WARN: caption = "Warning"; mbType = MB_ICONWARNING; break;
		case SPACONMSG_ERROR: caption = "Error"; mbType = MB_ICONHAND; break;
		case SPACONMSG_FATAL: caption = "Fatal error"; mbType = MB_ICONSTOP; break;
	}
	MessageBox(NULL, (cstr(name) + ": " + msg).c_str(), caption, mbType);
	return 0;
}
