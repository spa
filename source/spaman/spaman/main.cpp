#define WIN32_LEAN_AND_MEAN

#define _WIN32_WINNT 0x0502
#include <windows.h>

#include <tchar.h>

#include "..\cspamanagerimpl\cspamanagerimpl.h"
#include "..\..\..\include\spa\tools\cstrsearch.h"

cstr loadScript(char *fname) {
	HANDLE file = CreateFile(fname, FILE_READ_DATA, 0, NULL, OPEN_EXISTING, 0, NULL);
	if (file == INVALID_HANDLE_VALUE) {
		MessageBox(NULL, (cstr("Cannot open ") + fname).c_str(), "Error", MB_ICONHAND);
		return "";
	}
	char *buf = new char[513];
	unsigned long nread = 512;
	cstr result;
	while (nread == 512) {
		ReadFile(file, buf, 512, &nread, NULL);
		if (nread > 0) {
			buf[nread] = 0;
			result += buf;
		}
	}
	delete[] buf;
	CloseHandle(file);
	return result;
}



int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
#ifdef _DEBUG
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

	CSPAManagerImpl *man = new CSPAManagerImpl("SPA");

	TSPACMD *params = man->tokenize(GetCommandLine());
	cstr arg0 = cstr(params->token).extract_file_path(), arg1 = "n/a";
	cstr startScript;

	if (params->next != NULL && params->next->next != NULL && cstr(params->next->next->token) == "override") {
		startScript = loadScript(params->next->token);
	} else {
		SetCurrentDirectory(arg0.c_str());
		startScript = loadScript("start.sps");
	}
	if (params->next != NULL) {
		SetCurrentDirectory(cstr(params->next->token).extract_file_path().c_str());
		arg1 = params->next->token;
	} 

	man->delCmd(&params);

	startScript = cstrsearch(startScript).replace("arg0", arg0);
	startScript = cstrsearch(startScript).replace("arg1", arg1);

	man->AddRef();

	char *res = man->execute(startScript.c_str());
	man->delStr(&res);

	man->destroy();
	delete man;

	return 0;
}

