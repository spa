/*
 Simple Plugin Architecture
 aut: jv
 est: 061103

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#include "..\..\..\include\spa\cspacomponentcon\cspacomponentcon.h"

SPACALL CSPAComponentCon::CSPAComponentCon() {
	mCon = NULL;
	mCSCon = new CCrisec(CSSPINC);
	SPACMDREG(con, "Uses ISPAConsole implementing component identified by argument for further output. Usage: \"con component\"");
}

/*SPA commands definition*/
SPACMDDEF(CSPAComponentCon, con) {
	char *funid = "CSPAComponentCon::con: ";
	if (cmd == NULL || cmd->token == NULL) {
		logError((cstr(funid) + "Missing argument component.").c_str());
		return SERR;
	}
	ISPAComponent *comp = mMan->getComponent(cmd->token);
	if (comp == NULL) {
		logError((cstr(funid) + "Component \"" + cmd->token + "\" not found.").c_str());
		return SERR;
	}
	ISPAConsole *con;
	if (comp->QueryInterface(IID_ISPAConsole, (void**) &con) == E_NOINTERFACE) {
		mMan->releaseComponent((IUnknown**) &comp);
		logError((cstr(funid) + "Component \"" + cmd->token + "\" does not implement ISPAConsole.").c_str());
		return SERR;
	}
	mCSCon->enter();
	if (mCon != NULL) {
		mCon->Release();
		mMan->releaseComponent((IUnknown**) &mCon);
		/*
		 ISPAComponent *aux;
		 mcon->QueryInterface(IID_ISPAComponent, (void**)&aux);
		 mcon = con;
		 if (aux != NULL){
		 aux->Release();
		 mMan->releaseComponent((IUnknown**)&aux);
		 }
		 */
	}
	mCon = con;
	mCSCon->leave();
#if (SPALOGLVL >= SPALOGLVL_DEBUG)
	logInfo((cstr(funid) + "Console set to \"" + cmd->token + "\".").c_str());
#endif
	return SOK;
}

void STDMETHODCALLTYPE CSPAComponentCon::finish() {
	mCSCon->enter();
	/*
	 if (mcon != NULL){
	 mcon->Release();
	 ISPAComponent *aux;
	 mcon->QueryInterface(IID_ISPAComponent, (void**)&aux);
	 mcon = NULL;
	 if (aux != NULL) aux->Release();
	 mMan->releaseComponent((IUnknown**)&aux);
	 }
	 */
	if (mCon != NULL) {
		mCon->Release();
		mMan->releaseComponent((IUnknown**)&mCon);
	}
	mCSCon->leave();
}

