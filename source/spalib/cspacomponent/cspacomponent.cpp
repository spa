/*
 Simple Plugin Architecture
 aut: jv
 est: 060609

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#include "../../../include/spa/cspacomponent/cspacomponent.h"

#include <vector>
#include <algorithm>
#include <functional>

using namespace std;

char* sok = "ok";
char* serr = "err";

SPACALL CSPAComponent::CSPAComponent() {
//	mAbout = "CSPAComponent\r\nAuthor: JV\r\nVersion: 1.0.081231\r\nThis is basic C++ implementation of the ISPAComponent interface core functionality (reflection). If you get this as a result of about command, then the author of this component has yet to set the about string.";
	SPACOMPONENT_ABOUT("CSPAComponent", "JV", "This is basic C++ implementation of the ISPAComponent interface core functionality (reflection). If you get this as a result of about command, then the author of this component has yet to set the about string.");
	mCmdMap = new mTCMDMAP;
	mDefaultCmd = NULL;
	mDefaultCmdID = NULL;
	mRefCount = 0;
	mCSRef = new CCrisec(CSSPINC);

	SPACMDREG(about,"Returns a short piece of information about this component.");
	SPACMDREG(help,	"Use \"help silent\" to get list of commands returned. Use \"help command\" to get help for \"command\" printed in console. Use \"help silent command\" to get help for \"command\" returned.");
	// SPACMDREG(cmdList, "Lists all available commands.");
}

SPACALL CSPAComponent::~CSPAComponent() {
	if (mCmdMap == NULL)
		return;
	mTCMDMAPITER iter = mCmdMap->begin();
	while (iter != mCmdMap->end()) {
		if (iter->second != NULL) {
			delete iter->second;
			iter->second = NULL;
		}
		iter++;
	}
	delete mCmdMap;
	delete mCSRef;
}

/* IUnknown */

HRESULT STDMETHODCALLTYPE CSPAComponent::QueryInterface(REFIID riid, void** ppvObj) {
	if (IsEqualGUID(riid, IID_ISPAComponent)) {
		*ppvObj = static_cast<ISPAComponent*>(this);
		AddRef();
		return S_OK;
	}
	if (IsEqualGUID(riid, IID_IUnknown)) {
		*ppvObj = static_cast<IUnknown*>(this);
		AddRef();
		return S_OK;
	}
	*ppvObj = NULL;
	return E_NOINTERFACE;
}

ULONG STDMETHODCALLTYPE CSPAComponent::AddRef() {
	mCSRef->enter();
	mRefCount++;
	mCSRef->leave();
	return mRefCount;
}

ULONG STDMETHODCALLTYPE CSPAComponent::Release() {
	mCSRef->enter();
	mRefCount--;
	int mayDelete = mRefCount <= 0;
	mCSRef->leave();
	if (mayDelete) {delete this; return 0;}
	return mRefCount;
}

/* ISPAComponent */
char* STDMETHODCALLTYPE CSPAComponent::call(TSPACMD* cmd) {
	char *funid = "CSPAComponent::call: ";
	if (mCmdMap == NULL) {logFatal((cstr(funid) + "NULL mcmdMap").c_str()); return SERR;}
	if (cmd == NULL) {
		if (mDefaultCmd == NULL) {logError((cstr(funid) + "NULL in argument.").c_str()); return SERR;}
		return mDefaultCmd->call(NULL);
	}
	if (cmd->token == NULL) {
		if (mDefaultCmd == NULL) {logError((cstr(funid) + "NULL in argument.").c_str()); return SERR;}
		return mDefaultCmd->call(cmd);
	}
	mTCMDMAPITER iter = mCmdMap->find(cmd->token);
	if (iter == mCmdMap->end()) {
		if (mDefaultCmd == NULL) {logError((cstr(funid) + "Unknown command \"" + cmd->token + "\". Use \"" + mName + " help\".").c_str()); return SERR;}
		return mDefaultCmd->call(cmd);
	}
	return iter->second->call(cmd->next);
}

bool pcharLess(char* elem1, char* elem2) {
	return cstr(elem1) < elem2;
}

/*SPA commands definition*/

SPACMDDEF(CSPAComponent, about) {
	return mMan->newStr(mAbout);
}


SPACMDDEF(CSPAComponent, help) {
	int silent = 0;
	if (cmd != NULL && cstr("silent") == cmd->token) {
		silent = 1;
		cmd = cmd->next;
	}

	if (cmd == NULL || cmd->token == NULL) {
		cstr cmds;
		if (!silent)
			cmds = "Available commands: \r\n";

		vector<char*> auxVec;
		for (mTCMDMAPITER iter = mCmdMap->begin(); iter != mCmdMap->end(); iter++) {
			auxVec.push_back((char*) iter->first);
		}
		sort(auxVec.begin(), auxVec.end(), pcharLess);

		char *comma, *dot;
		if (!silent) {
			comma = ", ";
			dot = ".";
		} else {
			comma = " ";
			dot = "";
		}
		for (vector<char*>::iterator iter = auxVec.begin(); iter != auxVec.end(); iter++) {
			vector<char*>::iterator aux = iter;
			aux++;
			if (aux == auxVec.end())
				cmds += cstr(*iter) + dot;
			else
				cmds += cstr(*iter) + comma;
		}

		auxVec.clear();

		if (!silent) {
			if (mDefaultCmdID != NULL)
				cmds = cmds + " Default command: " + mDefaultCmdID + ".";
			logInfo((cstr("Use \"help command\" to get help for \"command\". ") + cmds).c_str());
			return SOK;
		} else
			return mMan->newStr(cmds.c_str());
	}

	mTCMDMAPITER iter = mCmdMap->find(cmd->token);
	if (iter == mCmdMap->end()) {
		logError((cstr("CSPAComponent::help: Unknown command \"") + cmd->token + "\".").c_str());
		return SERR;
	}
	if (!silent) {
		logInfo((cstr("Help for \"") + cmd->token + "\": " + iter->second->mHelp).c_str());
		return SOK;
	} else
		return mMan->newStr(iter->second->mHelp);
}