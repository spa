/*

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

*/


#include "..\..\..\include\spa\tools\cwaitable.h"

int SPACALL CWaitable::wait(unsigned int time) {
	int res = WaitForSingleObject(mHandle, time);
	switch (res) {
		case WAIT_OBJECT_0: return WR_OK;
		case WAIT_TIMEOUT: return WR_TIMEOUT;
		case WAIT_ABANDONED: return WR_ABANDONED;
		default: return WR_FAILED;
	}
}


int CWaitable::waitMultiple(unsigned int time, unsigned int* count, bool waitAll, ...) {
	HANDLE* handles = new HANDLE[*count];
	va_list list;
	va_start(list, waitAll);
	for (unsigned int i = 0; i < *count; i++) {
		CWaitable* waitable = va_arg(list, CWaitable*);
		if (waitable != NULL)
			handles[i] = waitable->mHandle;
		else
			handles[i] = NULL;
	}
	va_end(list);

	unsigned int res = WaitForMultipleObjects(*count, handles, waitAll ? TRUE : FALSE, time);

	delete[] handles;
	switch (res) {
	case WAIT_TIMEOUT:
		return WR_TIMEOUT;
	case WAIT_ABANDONED:
		return WR_ABANDONED;
	default:
		if (res >= WAIT_OBJECT_0 && res < WAIT_OBJECT_0 + *count) {
			*count = res - WAIT_OBJECT_0; 
			return WR_OK;
		}
		return WR_FAILED;
	}
}
