/*
extracted from libslack, see below
*/
/*
* libslack - http://libslack.org/
*
* Copyright (C) 2001 raf <raf@raf.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
* or visit http://www.gnu.org/copyleft/gpl.html
*
* 20040102 raf <raf@raf.org>
*/


#ifndef VSNPRINTF_VSSCANF_H
#define VSNPRINTF_VSSCANF_H


#include <locale.h>
#include <ctype.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>



#ifdef _NEED_VSNPRINTF


/* Define if we have long double */
#define HAVE_LONG_DOUBLE 1

/* Define if %p produces hex with a 0x prefix */
#define HAVE_PRINTF_PTR_FMT_ALTERNATE 1

/* Define if %p is treated almost like a signed conversion */
#define HAVE_PRINTF_PTR_FMT_SIGNED 1

/* Define if %p prints NULL as "(nil)" */
#define HAVE_PRINTF_PTR_FMT_NIL 1

/* Define if %s prints NULL as "(null)" */
#define HAVE_PRINTF_STR_FMT_NULL 1

/* Define if %g has a default precision of 1 (as specified by ISO C) */
/* #undef HAVE_PRINTF_FLT_FMT_G_STD */

/* Define if %p never produces a 0x prefix */
/* #undef HAVE_PRINTF_PTR_FMT_NOALT */


#ifdef HAVE_LONG_DOUBLE
#define LDOUBLE long double
#else
#define LDOUBLE double
#endif

/* Format read states */

#define PARSE_DEFAULT    0
#define PARSE_FLAGS      1
#define PARSE_WIDTH      2
#define PARSE_DOT        3
#define PARSE_PRECISION  4
#define PARSE_SIZE       5
#define PARSE_CONVERSION 6
#define PARSE_DONE       7

/* Format flags */

#define FLAG_MINUS      (1 << 0)
#define FLAG_PLUS       (1 << 1)
#define FLAG_SPACE      (1 << 2)
#define FLAG_ALT        (1 << 3)
#define FLAG_ZERO       (1 << 4)
#define FLAG_UP         (1 << 5)
#define FLAG_UNSIGNED   (1 << 6)
#define FLAG_F          (1 << 7)
#define FLAG_E          (1 << 8)
#define FLAG_G          (1 << 9)
#define FLAG_PTR_SIGNED (1 << 10)
#define FLAG_PTR_NIL    (1 << 11)
#define FLAG_PTR_NOALT  (1 << 12)

/* Conversion flags */

#define SIZE_SHORT   1
#define SIZE_LONG    2
#define SIZE_LDOUBLE 3


#ifndef max
#define max(a, b) ((a >= b) ? a : b)
#endif

static void outch(char *buffer, size_t *currlen, size_t *reqlen, size_t size, int c)
{
	if (*currlen + 1 < size)
		buffer[(*currlen)++] = (char)c;

	++*reqlen;
}

static void fmtch(char *buffer, size_t *currlen, size_t *reqlen, size_t size, int value, int flags, int width)
{
	int padlen;

	/*
	** A negative field width argument is taken as a - flag followed by a
	** positive field width argument
	*/

	if (width < 0)
	{
		flags |= FLAG_MINUS;
		width = -width;
	}

	padlen = width - 1;
	if (padlen < 0)
		padlen = 0;

	if (flags & FLAG_MINUS)
		padlen = -padlen;

	while (padlen > 0)
	{
		outch(buffer, currlen, reqlen, size, ' ');
		--padlen;
	}

	outch(buffer, currlen, reqlen, size, (unsigned int)value);

	while (padlen < 0)
	{
		outch(buffer, currlen, reqlen, size, ' ');
		++padlen;
	}
}

static void fmtstr(char *buffer, size_t *currlen, size_t *reqlen, size_t size, char *value, int flags, int width, int precision)
{
	int padlen, bytes;
	int cnt = 0;

	/*
	** A negative field width argument is taken as a - flag followed by a
	** positive field width argument
	*/

	if (width < 0)
	{
		flags |= FLAG_MINUS;
		width = -width;
	}

	/* A negative precision argument is taken as if the precision were omitted */

	if (precision < 0)
		precision = -1;

#ifdef HAVE_PRINTF_STR_FMT_NULL
	/* Print NULL as "(null)" if possible, otherwise as "" (like glibc) */

	if (value == NULL)
		value = (precision == -1 || precision >= 6) ? "(null)" : "";
#endif

	for (bytes = 0; (precision == -1 || bytes < precision) && value[bytes]; ++bytes)
	{}

	padlen = width - bytes;
	if (padlen < 0)
		padlen = 0;
	if (flags & FLAG_MINUS)
		padlen = -padlen;

	while (padlen > 0)
	{
		outch(buffer, currlen, reqlen, size, ' ');
		--padlen;
	}

	while (*value && (precision == -1 || (cnt < precision)))
	{
		outch(buffer, currlen, reqlen, size, *value++);
		++cnt;
	}

	while (padlen < 0)
	{
		outch(buffer, currlen, reqlen, size, ' ');
		++padlen;
	}
}

static void fmtint(char *buffer, size_t *currlen, size_t *reqlen, size_t size, long value, int base, int width, int precision, int flags)
{
	int signvalue = 0;
	unsigned long uvalue;
	unsigned char convert[22];
	int place = 0;
	int spadlen = 0;
	int zpadlen = 0;
	char *digits;
	int zextra = 0;
	int sextra = 0;

	/*
	** A negative field width argument is taken as a - flag followed by a
	** positive field width argument
	*/

	if (width < 0)
	{
#ifdef HAVE_PRINTF_WITH_SOLARIS_NEGATIVE_WIDTH_BEHAVIOUR
		if (!(flags & FLAG_ZERO) || precision >= 0)
#endif
			flags |= FLAG_MINUS;
		width = -width;
	}

	/* A negative precision argument is taken as if the precision were omitted */

	if (precision < 0)
		precision = -1;

	/* If the space and + flags both appear, the space flag will be ignored */

	if (flags & FLAG_PLUS)
		flags &= ~FLAG_SPACE;

	/* If the 0 and - flags both appear, the 0 flag will be ignored */

	if (flags & FLAG_MINUS)
		flags &= ~FLAG_ZERO;

	/* If a precision is specified, the 0 flag will be ignored */
	/* The d, i, o, u, x and X conversions have a default precision of 1 */

	if (precision == -1)
		precision = 1;
	else
		flags &= ~FLAG_ZERO;

	/*
	** The + flag: The result of the conversion will always begin with a plus
	** or minus sign. (It will begin with a sign only when a negative value is
	** converted if this flag is not specified.)
	**
	** The space flag: If the first character of a signed conversion is not a
	** sign, or if a signed conversion results in no characters, a space will
	** be prefixed to the result.
	*/

	uvalue = (unsigned long)value;

	if (!(flags & FLAG_UNSIGNED))
	{
		if (value < 0)
		{
			signvalue = '-';
			uvalue = (unsigned long)-value;
		}
		else if (flags & FLAG_PLUS)
			signvalue = '+';
		else if (flags & FLAG_SPACE)
			signvalue = ' ';
	}

#ifdef HAVE_PRINTF_PTR_FMT_SIGNED
	/* Behave like glibc (treat %p as signed (almost)) */
	if (flags & FLAG_PTR_SIGNED && value)
	{
		if (flags & FLAG_PLUS)
			signvalue = '+';
		else if (flags & FLAG_SPACE)
			signvalue = ' ';
	}
#endif

#ifdef HAVE_PRINTF_PTR_FMT_NOALT
	/* Behave like Solaris - %p never produces 0x */
	if (flags & FLAG_PTR_NOALT)
		flags &= ~FLAG_ALT;
#endif

#ifdef HAVE_PRINTF_PTR_FMT_NIL
	if (flags & FLAG_PTR_NIL && !value)
	{
		spadlen = width - 5;
		if (spadlen < 0)
			spadlen = 0;
		if (flags & FLAG_MINUS)
			spadlen = -spadlen;

		while (spadlen > 0)
		{
			outch(buffer, currlen, reqlen, size, ' ');
			--spadlen;
		}

		outch(buffer, currlen, reqlen, size, '(');
		outch(buffer, currlen, reqlen, size, 'n');
		outch(buffer, currlen, reqlen, size, 'i');
		outch(buffer, currlen, reqlen, size, 'l');
		outch(buffer, currlen, reqlen, size, ')');

		while (spadlen < 0)
		{
			outch(buffer, currlen, reqlen, size, ' ');
			++spadlen;
		}

		return;
	}
#endif

	/* %[XEG] produce uppercase alpha characters */

	digits = (flags & FLAG_UP) ? "0123456789ABCDEF" : "0123456789abcdef";

	/*
	** The result of converting a zero value with a precision of zero is no
	** characters
	*/

	if (precision || uvalue)
	{
		do
		{
			convert[place++] = digits[uvalue % base];
			uvalue /= base;
		}
		while (uvalue && (place < 21));
	}

	convert[place] = 0;

	/*
	** The # flag: For o conversion, it increases the precision to force the
	** first digit of the result to be a zero. For x (or X) conversion, a
	** non-zero result will have 0x (or 0X) prefixed to it.
	*/

	if (flags & FLAG_ALT)
	{
		if (base == 16 && value)
			sextra = 2;
#ifdef HAVE_PRINTF_WITH_SOLARIS8_ZERO_PRECISION_ALT_OCTAL_BEHAVIOUR
		else if (base == 8 && precision <= place && (place != 0 && convert[place - 1] != '0'))
			zextra = 1;
#else
		else if (base == 8 && precision <= place && (place == 0 || convert[place - 1] != '0'))
			zextra = 1;
#endif
	}

	/*
	** The 0 flag: Leading zeroes (following any indication of sign or base)
	** are used to pad to the field width; no space padding is performed.
	*/

	zpadlen = precision - place;
	if (zpadlen < 0)
		zpadlen = 0;
	zpadlen += zextra;

	spadlen = width - (place + zpadlen) - (signvalue ? 1 : 0) - sextra;
	if (spadlen < 0)
		spadlen = 0;

	if (flags & FLAG_ZERO)
	{
		zpadlen = max(zpadlen, spadlen);
		spadlen = 0;
	}

	if (flags & FLAG_MINUS)
		spadlen = -spadlen;

	while (spadlen > 0)
	{
		outch(buffer, currlen, reqlen, size, ' ');
		--spadlen;
	}

	if (signvalue)
		outch(buffer, currlen, reqlen, size, signvalue);

	if (flags & FLAG_ALT && base == 16 && sextra == 2)
	{
		outch(buffer, currlen, reqlen, size, '0');
		outch(buffer, currlen, reqlen, size, (flags & FLAG_UP) ? 'X' : 'x');
	}

	while (zpadlen > 0)
	{
		outch(buffer, currlen, reqlen, size, '0');
		--zpadlen;
	}

	while (place > 0)
		outch(buffer, currlen, reqlen, size, convert[--place]);

	while (spadlen < 0)
	{
		outch(buffer, currlen, reqlen, size, ' ');
		++spadlen;
	}
}

#ifndef DBL_MAX_10_EXP
#define DBL_MAX_10_EXP 308
#endif

#ifndef LDBL_MAX_10_EXP
#define LDBL_MAX_10_EXP 4932
#endif

#define DBL_MAX_EXP_DIGITS 3
#define LDBL_MAX_EXP_DIGITS 4

#define MAX_DIGITS(ldbl) \
	((ldbl) ? LDBL_MAX_10_EXP : DBL_MAX_10_EXP)
#define MAX_EXP_DIGITS(ldbl) \
	((ldbl) ? LDBL_MAX_EXP_DIGITS : DBL_MAX_EXP_DIGITS)
#define F_MAX(width, precision, ldbl) \
	max((width), 1 + MAX_DIGITS(ldbl) + 1 + (precision))
#define E_MAX(width, precision, ldbl) \
	max((width), 3 + (precision) + 2 + MAX_EXP_DIGITS(ldbl))
#define G_MAX(width, precision, ldbl) \
	max(F_MAX((width), precision, ldbl), E_MAX((width), precision, ldbl))

static void fmtfp(char *buffer, size_t *currlen, size_t *reqlen, size_t size, LDOUBLE fvalue, int width, int precision, int flags, int cflags)
{
	/* Calculate maximum space required and delegate to sprintf() */

	char buf[512], *convert = buf, *p;
	char format[1 + 5 + 20 + 1 + 20 + 1 + 1 + 1];
	char widstr[21];
	int fpmax;

	/*
	** A negative field width argument is taken as a - flag followed by a
	** positive field width argument
	*/

	if (width < 0)
	{
#ifdef HAVE_PRINTF_WITH_SOLARIS_NEGATIVE_WIDTH_BEHAVIOUR
		if ((flags & FLAG_ZERO) == 0)
#endif
			flags |= FLAG_MINUS;
		width = -width;
	}

	/* A negative precision argument is taken as if the precision were omitted */

	if (precision < 0)
		precision = -1;

	/*
	** The ISO C standard (Section 7.9.6.2, The fscanf function), says that
	** if the precision is missing, the default precision for f, e and E is
	** taken to be 6. The default precision for g and G is taken to be 1.
	** However, many systems use 6, so we'll provide an option.
	*/

	if (precision == -1)
		precision =
#ifdef HAVE_PRINTF_FLT_FMT_G_STD
			(flags & FLAG_G) ? 1 :
#endif
			6;

	/* Calculate maximum possible length */

	if (flags & FLAG_F)
		fpmax = F_MAX(width, precision, (cflags & SIZE_LDOUBLE)) + 1;
	else if (flags & FLAG_E)
		fpmax = E_MAX(width, precision, (cflags & SIZE_LDOUBLE)) + 1;
	else
		fpmax = G_MAX(width, precision, (cflags & SIZE_LDOUBLE)) + 1;

	/* Ensure enough space or bail out */

//	if (fpmax > 512 && !(convert = malloc(fpmax)))
	if (fpmax > 512 && !(convert = new char[fpmax]))
		return;

	/* Build the format string (probably should have just copied it) */

	sprintf(widstr, "%d", width);
	sprintf(format, "%%%s%s%s%s%s%s.%d%s%s",
		(flags & FLAG_MINUS) ? "-" : "",
		(flags & FLAG_PLUS)  ? "+" : "",
		(flags & FLAG_SPACE) ? " " : "",
		(flags & FLAG_ALT)   ? "#" : "",
		(flags & FLAG_ZERO)  ? "0" : "",
		(width) ? widstr : "",
		precision,
		(cflags & SIZE_LDOUBLE) ? "L" : "",
		(flags & FLAG_E) ? (flags & FLAG_UP) ? "E" : "e" :
		(flags & FLAG_G) ? (flags & FLAG_UP) ? "G" : "g" : "f"
	);

	/* Delegate to sprintf() */

	if (cflags & SIZE_LDOUBLE)
		sprintf(convert, format, fvalue);
	else
		sprintf(convert, format, (double)fvalue);

	/* Copy to buffer */

	for (p = convert; *p; ++p)
		outch(buffer, currlen, reqlen, size, *p);

	/* Deallocate if necessary */

	if (convert != buf) delete[] convert;
//		free(convert);
}





int vsnprintf(char *str, size_t size, const char *format, va_list args)
{
	char ch;
	long value;
	LDOUBLE fvalue;
	char *strvalue;
	int width;
	int precision;
	int state;
	int flags;
	int cflags;
	size_t currlen;
	size_t reqlen;

	state = PARSE_DEFAULT;
	currlen = reqlen = flags = cflags = width = 0;
	precision = -1;
	ch = *format++;

	while (state != PARSE_DONE)
	{
		if (ch == '\0')
		{
			if (state == PARSE_DEFAULT)
				state = PARSE_DONE;
			else
				return -1;
		}

		switch (state)
		{
			case PARSE_DEFAULT:
				if (ch == '%')
					state = PARSE_FLAGS;
				else
					outch(str, &currlen, &reqlen, size, ch);
				ch = *format++;
				break;

			case PARSE_FLAGS:
				switch (ch)
				{
					case '-':
						flags |= FLAG_MINUS;
						ch = *format++;
						break;

					case '+':
						flags |= FLAG_PLUS;
						ch = *format++;
						break;

					case ' ':
						flags |= FLAG_SPACE;
						ch = *format++;
						break;

					case '#':
						flags |= FLAG_ALT;
						ch = *format++;
						break;

					case '0':
						flags |= FLAG_ZERO;
						ch = *format++;
						break;

					default:
						state = PARSE_WIDTH;
						break;
				}
				break;

			case PARSE_WIDTH:
				if (isdigit((int)(unsigned char)ch))
				{
					width = 10 * width + ch - '0';
					ch = *format++;
				}
				else if (ch == '*')
				{
					width = va_arg(args, int);
					ch = *format++;
					state = PARSE_DOT;
				}
				else
					state = PARSE_DOT;
				break;

			case PARSE_DOT:
				if (ch == '.')
				{
					precision = 0;
					state = PARSE_PRECISION;
					ch = *format++;
				}
				else
					state = PARSE_SIZE;
				break;

			case PARSE_PRECISION:
				if (isdigit((int)(unsigned char)ch))
				{
					precision = 10 * precision + ch - '0';
					ch = *format++;
				}
				else if (ch == '*')
				{
					precision = va_arg(args, int);
					ch = *format++;
					state = PARSE_SIZE;
				}
				else
					state = PARSE_SIZE;
				break;

			case PARSE_SIZE:
				switch (ch)
				{
					case 'h':
						cflags = SIZE_SHORT;
						ch = *format++;
						break;

					case 'l':
						cflags = SIZE_LONG;
						ch = *format++;
						break;

					case 'L':
						cflags = SIZE_LDOUBLE;
						ch = *format++;
						break;
				}

				state = PARSE_CONVERSION;
				break;

			case PARSE_CONVERSION:
				switch (ch)
				{
					case 'd':
					case 'i':
						if (cflags == SIZE_SHORT)
							value = (short)va_arg(args, int);
						else if (cflags == SIZE_LONG)
							value = (long)va_arg(args, int);
						else
							value = (int)va_arg(args, int);

						fmtint(str, &currlen, &reqlen, size, value, 10, width, precision, flags);
						break;

					case 'o':
						flags |= FLAG_UNSIGNED;
						if (cflags == SIZE_SHORT)
							value = (unsigned short)va_arg(args, int);
						else if (cflags == SIZE_LONG)
							value = (unsigned long)va_arg(args, int);
						else
							value = (unsigned int)va_arg(args, int);

						fmtint(str, &currlen, &reqlen, size, value, 8, width, precision, flags);
						break;

					case 'u':
						flags |= FLAG_UNSIGNED;
						if (cflags == SIZE_SHORT)
							value = (unsigned short)va_arg(args, int);
						else if (cflags == SIZE_LONG)
							value = (unsigned long)va_arg(args, int);
						else
							value = (unsigned int)va_arg(args, int);

						fmtint(str, &currlen, &reqlen, size, value, 10, width, precision, flags);
						break;

					case 'X':
						flags |= FLAG_UP;
					case 'x':
						flags |= FLAG_UNSIGNED;
						if (cflags == SIZE_SHORT)
							value = (unsigned short)va_arg(args, int);
						else if (cflags == SIZE_LONG)
							value = (unsigned long)va_arg(args, int);
						else
							value = (unsigned int)va_arg(args, int);

						fmtint(str, &currlen, &reqlen, size, value, 16, width, precision, flags);
						break;

					case 'f':
						flags |= FLAG_F;
						if (cflags == SIZE_LDOUBLE)
							fvalue = va_arg(args, LDOUBLE);
						else
							fvalue = (LDOUBLE)va_arg(args, double);

						fmtfp(str, &currlen, &reqlen, size, fvalue, width, precision, flags, cflags);
						break;

					case 'E':
						flags |= FLAG_UP;
					case 'e':
						flags |= FLAG_E;
						if (cflags == SIZE_LDOUBLE)
							fvalue = va_arg(args, LDOUBLE);
						else
							fvalue = (LDOUBLE)va_arg(args, double);

						fmtfp(str, &currlen, &reqlen, size, fvalue, width, precision, flags, cflags);
						break;

					case 'G':
						flags |= FLAG_UP;
					case 'g':
						flags |= FLAG_G;
						if (cflags == SIZE_LDOUBLE)
							fvalue = va_arg(args, LDOUBLE);
						else
							fvalue = (LDOUBLE)va_arg(args, double);

						fmtfp(str, &currlen, &reqlen, size, fvalue, width, precision, flags, cflags);
						break;

					case 'c':
						fmtch(str, &currlen, &reqlen, size, va_arg(args, int), flags, width);
						break;

					case 's':
						strvalue = va_arg(args, char *);
						fmtstr(str, &currlen, &reqlen, size, strvalue, flags, width, precision);
						break;

					case 'p':
						flags |= FLAG_UNSIGNED;
#ifdef HAVE_PRINTF_PTR_FMT_ALTERNATE
						flags |= FLAG_ALT;
#endif
#ifdef HAVE_PRINTF_PTR_FMT_SIGNED
						flags |= FLAG_PTR_SIGNED;
#endif
#ifdef HAVE_PRINTF_PTR_FMT_NIL
						flags |= FLAG_PTR_NIL;
#endif
#ifdef HAVE_PRINTF_PTR_FMT_NOALT
						flags |= FLAG_PTR_NOALT;
#endif
						cflags = SIZE_LONG;
						strvalue = (char*) va_arg(args, void *);
						fmtint(str, &currlen, &reqlen, size, (long)strvalue, 16, width, precision, flags);
						break;

					case 'n':
						if (cflags == SIZE_SHORT)
						{
							short *num = va_arg(args, short *);
							*num = (short)currlen;
						}
						else if (cflags == SIZE_LONG)
						{
							long *num = va_arg(args, long *);
							*num = (long)currlen;
						}
						else
						{
							int *num = va_arg(args, int *);
							*num = currlen;
						}

						break;

					case '%':
						if (flags != 0 || cflags != 0 || width != 0 || precision != 0)
							return -1;

						outch(str, &currlen, &reqlen, size, ch);
						break;

					default:
						return -1;
				}

				ch = *format++;
				state = PARSE_DEFAULT;
				flags = cflags = width = 0;
				precision = -1;
				break;

			case PARSE_DONE:
				break;
		}
	}

	if (size)
		str[currlen] = '\0';

	return reqlen;
}



#endif //_NEED_VSNPRINTF







int vsscanf(const char *str, const char *format, va_list args)
{
	const char *f, *s;
	const char point = localeconv()->decimal_point[0];
	int cnv = 0;

	for (s = str, f = format; *f; ++f)
	{
		if (*f == '%')
		{
			int size = 0;
			int width = 0;
			int do_cnv = 1;

			if (*++f == '*')
				++f, do_cnv = 0;

			for (; isdigit((int)(unsigned int)*f); ++f)
				width *= 10, width += *f - '0';

			if (*f == 'h' || *f == 'l' || *f == 'L')
				size = *f++;

			if (*f != '[' && *f != 'c' && *f != 'n')
				while (isspace((int)(unsigned int)*s))
					++s;

#define COPY                         *b++ = *s++, --width
#define MATCH(cond)                  if (width && (cond)) COPY;
#define MATCH_ACTION(cond, action)   if (width && (cond)) { COPY; action; }
#define MATCHES_ACTION(cond, action) while (width && (cond)) { COPY; action; }
#define FAIL                         (cnv) ? cnv : EOF
			switch (*f)
			{
				case 'd': case 'i': case 'o': case 'u': case 'x': case 'X':
				case 'p':
				{
					static const char types[] = "diouxXp";
					static const int bases[] = { 10, 0, 8, 10, 16, 16, 16 };
					static const char digitset[] = "0123456789abcdefABCDEF";
					static const int setsizes[] = { 10, 0, 0, 0, 0, 0, 0, 0, 8, 0, 10, 0, 0, 0, 0, 0, 22 };
					int base = bases[strchr(types, *f) - types];
					int setsize;
					char buf[513];
					char *b = buf;
					int digit = 0;
					if (width <= 0 || width > 512) width = 512;
					MATCH(*s == '+' || *s == '-')
					MATCH_ACTION(*s == '0',
						digit = 1;
						MATCH_ACTION((*s == 'x' || *s == 'X') && (base == 0 || base == 16), base = 16) else base = 8;
					)
					setsize = setsizes[base];
					MATCHES_ACTION(memchr(digitset, *s, setsize), digit = 1)
					if (!digit) return FAIL;
					*b = '\0';
					if (do_cnv)
					{
						if (*f == 'd' || *f == 'i')
						{
							long data = strtol(buf, NULL, base);
							if (size == 'h')
								*va_arg(args, short *) = (short)data;
							else if (size == 'l')
								*va_arg(args, long *) = data;
							else
								*va_arg(args, int *) = (int)data;
						}
						else
						{
							unsigned long data = strtoul(buf, NULL, base);
							if (size == 'p')
								*va_arg(args, void **) = (void *)data;
							else if (size == 'h')
								*va_arg(args, unsigned short *) = (unsigned short)data;
							else if (size == 'l')
								*va_arg(args, unsigned long *) = data;
							else
								*va_arg(args, unsigned int *) = (unsigned int)data;
						}
						++cnv;
					}
					break;
				}

				case 'e': case 'E': case 'f': case 'g': case 'G':
				{
					char buf[513];
					char *b = buf;
					int digit = 0;
					if (width <= 0 || width > 512) width = 512;
					MATCH(*s == '+' || *s == '-')
					MATCHES_ACTION(isdigit((int)(unsigned int)*s), digit = 1)
					MATCH(*s == point)
					MATCHES_ACTION(isdigit((int)(unsigned int)*s), digit = 1)
					MATCHES_ACTION(digit && (*s == 'e' || *s == 'E'),
						MATCH(*s == '+' || *s == '-')
						digit = 0;
						MATCHES_ACTION(isdigit((int)(unsigned int)*s), digit = 1)
					)
					if (!digit) return FAIL;
					*b = '\0';
					if (do_cnv)
					{
						double data = strtod(buf, NULL);
						if (size == 'l')
							*va_arg(args, double *) = data;
						else if (size == 'L')
							*va_arg(args, long double *) = (long double)data;
						else
							*va_arg(args, float *) = (float)data;
						++cnv;
					}
					break;
				}

				case 's':
				{
					char *arg = va_arg(args, char *);
					if (width <= 0) width = INT_MAX;
					while (width-- && *s && !isspace((int)(unsigned int)*s))
						if (do_cnv) *arg++ = *s++;
					if (do_cnv) *arg = '\0', ++cnv;
					break;
				}

				case '[':
				{
					char *arg = va_arg(args, char *);
					int setcomp = 0;
					size_t setsize;
					const char *end;
					if (width <= 0) width = INT_MAX;
					if (*++f == '^') setcomp = 1, ++f;
					end = strchr((*f == ']') ? f + 1 : f, ']');
					if (!end) return FAIL; /* Could be cnv to match glibc-2.2 */
					setsize = end - f;     /* But FAIL matches the C standard */
					while (width-- && *s)
					{
						if (!setcomp && !memchr(f, *s, setsize)) break;
						if (setcomp && memchr(f, *s, setsize)) break;
						if (do_cnv) *arg++ = *s++;
					}
					if (do_cnv) *arg = '\0', ++cnv;
					f = end;
					break;
				}

				case 'c':
				{
					char *arg = va_arg(args, char *);
					if (width <= 0) width = 1;
					while (width--)
					{
						if (!*s) return FAIL;
						if (do_cnv) *arg++ = *s++;
					}
					if (do_cnv) ++cnv;
					break;
				}

				case 'n':
				{
					if (size == 'h')
						*va_arg(args, short *) = (short)(s - str);
					else if (size == 'l')
						*va_arg(args, long *) = (long)(s - str);
					else
						*va_arg(args, int *) = (int)(s - str);
					break;
				}

				case '%':
				{
					if (*s++ != '%') return cnv;
					break;
				}

				default:
					return FAIL;
			}
		}
		else if (isspace((int)(unsigned int)*f))
		{
			while (isspace((int)(unsigned int)f[1]))
				++f;
			while (isspace((int)(unsigned int)*s))
				++s;
		}
		else
		{
			if (*s++ != *f)
				return cnv;
		}
	}

	return cnv;
}

#endif