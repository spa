/*
	String wrapper, char* only, copy-on-write implementation,
	hopefully portable, further development intended (more utility functions etc.)
	
	Version 2.0
	
	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.


	Copyright (C) 2002 - 2005 Jan Vanek (honza.vanek@gmail.com)
	
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
	or visit http://www.gnu.org/copyleft/gpl.html
	
	Portions Copyright (C) 2001 by raf <raf@raf.org> (libslack)
*/


#include <ctype.h>
#include <stdio.h>
#include <memory.h>
#include <string.h>

#include "..\..\..\include\spa\tools\cstr.h"

CSTRCALL cstr::~cstr() {
	m_destroy_data();
}

void CSTRCALL cstr::m_fork(void) {
	if (m_data != NULL) {
		if (m_data->ref>0) {
			m_data->ref--;
			m_data = new TSHARED;
			if (m_data == NULL) return;
			m_data->l = m_data->alloc = m_data->ref = 0;
			m_data->buf = NULL;
		}
	} else {
		m_data = new TSHARED;
		if (m_data == NULL) return;
		m_data->l = m_data->alloc = m_data->ref = 0;
		m_data->buf = NULL;
	}
}

void CSTRCALL cstr::m_fork_copy(int length) {
	unsigned int alloc = MINSIZE;
	while (alloc<length) alloc<<=1;
	if (m_data != NULL) {
		if (m_data->ref>0) {
			m_data->ref--;
			TSHARED *bak_data = m_data;
			m_data = new TSHARED;
			if (m_data == NULL) return;
			m_data->ref = 0;
			m_data->l = length;
			if (bak_data->alloc>alloc) m_data->alloc = bak_data->alloc;
			else m_data->alloc = alloc;
			m_data->buf = new char[alloc];
			if (m_data->buf == NULL) {m_data->l = m_data->alloc = 0; return;}
			memcpy(m_data->buf, bak_data->buf, bak_data->l);
			m_data->buf[length-1] = 0;
		} else {
			if (m_data->alloc<alloc) {
				m_data->alloc = alloc;
				char *bak_buf = m_data->buf;
				if (m_data->buf != NULL) {
					m_data->buf = new char[alloc];
					if (m_data->buf == NULL) {m_data->l = m_data->alloc = 0; return;}
					memcpy(m_data->buf, bak_buf, m_data->l);
					delete[] bak_buf;
				}
			}
			m_data->l = length;
			m_data->buf[length-1] = 0;
		}
	} else {
		m_data = new TSHARED;
		if (m_data == NULL) return;
		m_data->l = length;
		m_data->ref = 0;
		m_data->alloc = alloc;
		m_data->buf = new char[alloc];
		if (m_data->buf == NULL) {m_data->l = m_data->alloc = 0; return;}
		m_data->buf[length-1] = 0;
	}
}

void CSTRCALL cstr::m_set_length(int length) {
	m_fork();
	unsigned int alloc = MINSIZE;
	while (alloc<length) alloc<<=1;
	if (m_data->alloc == alloc) {
		m_data->l = length;
		m_data->buf[length-1] = 0;
		return;
	}
	if (m_data->buf != NULL) delete[] m_data->buf;
	m_data->alloc = alloc;
	m_data->l = length;
	m_data->buf = new char[alloc];
	if (m_data->buf == NULL) {m_data->l = m_data->alloc = 0; return;}
	m_data->buf[length-1] = 0;
}

void CSTRCALL cstr::m_destroy_data(void) {
	if (m_data != NULL)
	if (m_data->ref>0) {m_data->ref--; m_data = NULL;}
	else {
		if (m_data->buf != NULL) delete[] m_data->buf;
		delete m_data;
		m_data = NULL;
	}
}

CSTRCALL cstr::cstr(const char* str) {
	m_data = NULL;
	if (str == NULL)
		return;
	unsigned int i = 0;
	while (str[i++])
		;
	m_set_length(i);
	if (m_data == NULL || m_data->buf == NULL)
		return;
	memcpy(m_data->buf, str, i);
}

cstr& CSTRCALL cstr::operator= (const cstr& rhs) {
	m_destroy_data();
	if (rhs.m_data != NULL) rhs.m_data->ref++; m_data = rhs.m_data;
	return *this;
}

cstr& CSTRCALL cstr::operator+= (const cstr rhs) {
	if (rhs.m_data == NULL || rhs.m_data->l == 0 || rhs.m_data->buf == NULL) return *this;
	unsigned int bak_l = length();
	m_fork_copy(bak_l + rhs.m_data->l);
	if (m_data->buf == NULL) return *this;
	memcpy(m_data->buf + bak_l, rhs.m_data->buf, rhs.m_data->l);
	return *this;
}

cstr& CSTRCALL cstr::operator+= (const char* rhs) {
	if (rhs == NULL) return *this;
	unsigned int bak_l = length(), rhs_l;
	for (rhs_l = 0; rhs[rhs_l] != 0; rhs_l++);
	rhs_l++;
	m_fork_copy(bak_l + rhs_l);
	if (m_data->buf == NULL) return *this;
	memcpy(m_data->buf + bak_l, rhs, rhs_l);
	return *this;
}

cstr& CSTRCALL cstr::operator+= (char rhs)
{
	if (!m_data)
		m_fork_copy(2);
	else
		m_fork_copy(m_data->l + 1);
	m_data->buf[m_data->l - 2] = rhs;
	m_data->buf[m_data->l - 1] = 0;
	return *this;
}

cstr CSTRCALL cstr::operator+ (const cstr& rhs) const {
	if (m_data == NULL || m_data->buf == NULL) return rhs;
	if (rhs.m_data == NULL || rhs.m_data->buf == NULL) return *this;
	cstr hlp;
	hlp.m_set_length(m_data->l + rhs.m_data->l-1);
	if (hlp.m_data == NULL || hlp.m_data->buf == NULL) return hlp;
	memcpy(hlp.m_data->buf, m_data->buf, m_data->l-1);
	memcpy(hlp.m_data->buf + (m_data->l-1), rhs.m_data->buf, rhs.m_data->l);
	return hlp;
}

cstr CSTRCALL cstr::operator+ (const char* rhs) const {
	if (m_data == NULL || m_data->buf == NULL) return cstr(rhs);
	if (rhs == NULL) return *this;
	unsigned int i = 0;
	while (rhs[i++]);
	cstr hlp;
	hlp.m_set_length(m_data->l + i-1);
	if (hlp.m_data == NULL || hlp.m_data->buf == NULL) return hlp;
	memcpy(hlp.m_data->buf, m_data->buf, m_data->l-1);
	memcpy(hlp.m_data->buf + (m_data->l-1), rhs, i);
	return hlp;
}

bool CSTRCALL cstr::operator== (const cstr& rhs) const {
	char *hlhs, *hrhs;
	if (m_data == NULL || m_data->buf == NULL) hlhs = ""; else hlhs = m_data->buf;
	if (rhs.m_data == NULL || rhs.m_data->buf == NULL) hrhs = ""; else hrhs = rhs.m_data->buf;
	return !strcmp(hlhs, hrhs);
}

bool CSTRCALL cstr::operator!= (const cstr& rhs) const {
	char *hlhs, *hrhs;
	if (m_data == NULL || m_data->buf == NULL) hlhs = ""; else hlhs = m_data->buf;
	if (rhs.m_data == NULL || rhs.m_data->buf == NULL) hrhs = ""; else hrhs = rhs.m_data->buf;
	return strcmp(hlhs, hrhs);
}

bool CSTRCALL cstr::operator< (const cstr& rhs) const {
	char *hlhs, *hrhs;
	if (m_data == NULL || m_data->buf == NULL) hlhs = ""; else hlhs = m_data->buf;
	if (rhs.m_data == NULL || rhs.m_data->buf == NULL) hrhs = ""; else hrhs = rhs.m_data->buf;
	return strcmp(hlhs, hrhs)<0;
}

bool CSTRCALL cstr::operator> (const cstr& rhs) const {
	char *hlhs, *hrhs;
	if (m_data == NULL || m_data->buf == NULL) hlhs = ""; else hlhs = m_data->buf;
	if (rhs.m_data == NULL || rhs.m_data->buf == NULL) hrhs = ""; else hrhs = rhs.m_data->buf;
	return strcmp(hlhs, hrhs)>0;
}

bool CSTRCALL cstr::operator<= (const cstr& rhs) const {
	char *hlhs, *hrhs;
	if (m_data == NULL || m_data->buf == NULL) hlhs = ""; else hlhs = m_data->buf;
	if (rhs.m_data == NULL || rhs.m_data->buf == NULL) hrhs = ""; else hrhs = rhs.m_data->buf;
	return strcmp(hlhs, hrhs) <= 0;
}

bool CSTRCALL cstr::operator>= (const cstr& rhs) const {
	char *hlhs, *hrhs;
	if (m_data == NULL || m_data->buf == NULL) hlhs = ""; else hlhs = m_data->buf;
	if (rhs.m_data == NULL || rhs.m_data->buf == NULL) hrhs = ""; else hrhs = rhs.m_data->buf;
	return strcmp(hlhs, hrhs) >= 0;
}

bool CSTRCALL cstr::operator== (const char* rhs) const {
	char *hlhs, *hrhs;
	if (m_data == NULL || m_data->buf == NULL) hlhs = ""; else hlhs = m_data->buf;
	if (rhs == NULL) hrhs = ""; else hrhs = (char*)rhs;
	return !strcmp(hlhs, hrhs);
}

bool CSTRCALL cstr::operator!= (const char* rhs) const {
	char *hlhs, *hrhs;
	if (m_data == NULL || m_data->buf == NULL) hlhs = ""; else hlhs = m_data->buf;
	if (rhs == NULL) hrhs = ""; else hrhs = (char*)rhs;
	return strcmp(hlhs, hrhs);
}

bool CSTRCALL cstr::operator< (const char* rhs) const {
	char *hlhs, *hrhs;
	if (m_data == NULL || m_data->buf == NULL) hlhs = ""; else hlhs = m_data->buf;
	if (rhs == NULL) hrhs = ""; else hrhs = (char*)rhs;
	return strcmp(hlhs, hrhs)<0;
}

bool CSTRCALL cstr::operator> (const char* rhs) const {
	char *hlhs, *hrhs;
	if (m_data == NULL || m_data->buf == NULL) hlhs = ""; else hlhs = m_data->buf;
	if (rhs == NULL) hrhs = ""; else hrhs = (char*)rhs;
	return strcmp(hlhs, hrhs)>0;
}

bool CSTRCALL cstr::operator<= (const char* rhs) const {
	char *hlhs, *hrhs;
	if (m_data == NULL || m_data->buf == NULL) hlhs = ""; else hlhs = m_data->buf;
	if (rhs == NULL) hrhs = ""; else hrhs = (char*)rhs;
	return strcmp(hlhs, hrhs) <= 0;
}

bool CSTRCALL cstr::operator>= (const char* rhs) const {
	char *hlhs, *hrhs;
	if (m_data == NULL || m_data->buf == NULL) hlhs = ""; else hlhs = m_data->buf;
	if (rhs == NULL) hrhs = ""; else hrhs = (char*)rhs;
	return strcmp(hlhs, hrhs) >= 0;
}



cstr CSTRCALL cstr::operator()(const unsigned int from, const unsigned int to) const {
	if (m_data == NULL || m_data->buf == NULL || to >= m_data->l - 1 || from > to) return cstr();
	cstr result;
	result.m_set_length(to - from + 2);
	if (result.m_data == NULL || result.m_data->buf == NULL) return cstr();
	memcpy(result.m_data->buf, m_data->buf + from, to - from + 1);
	return result;
}







#ifdef _MSC_VER
#define vsnprintf _vsnprintf
#if defined _DEBUG || defined _RUNTIME_IN_DLL
#include "vsnprintf_vsscanf.h"
#else
extern "C" int __cdecl _input_l(FILE*, const char*, _locale_t, va_list);
#endif /* if defined _DEBUG */
#endif /* ifdef _MSC_VER */

int CSTRCALL cstr::vprintf(const char* format, va_list paramList) {
	int size = vsnprintf(NULL, 0, format, paramList) + 1;
	m_set_length(size);
	if (m_data == NULL || m_data->buf == NULL) return 0;
	return vsnprintf(m_data->buf, size, format, paramList);
}

int cstr::printf(const char* format, ...) {
	int res;
	va_list paramList;
	va_start(paramList, format);
	res = vprintf(format, paramList);
	va_end(paramList);
	return res;
}

cstr cstr::sprintf(const char* format, ...) {
	cstr tmp;
	va_list paramList;
	va_start(paramList, format);
	tmp.vprintf(format, paramList);
	va_end(paramList);
	return tmp;
}

int cstr::scanf(const char* format, ...) const {
	if (m_data == NULL || m_data->buf == NULL)
		return 0;
	int res;
	va_list paramList;
	va_start(paramList, format);
#if defined _MSC_VER && !defined _DEBUG && !defined _RUNTIME_IN_DLL
	FILE str;
	str._flag = _IOREAD|_IOSTRG|_IOMYBUF;
	str._ptr = str._base = m_data->buf;
	str._cnt = m_data->l;
	res = _input_l(&str, format, 0, paramList);
#else
	res = vsscanf(m_data->buf, format, paramList);
#endif
	va_end(paramList);
	return res;
}

int CSTRCALL cstr::to_int(void) const {
	int res = 0;
	scanf("%i", &res);
	return res;
}

int CSTRCALL cstr::to_int(int defaultValue) const {
	int res = 0;
	if (scanf("%i", &res) == 1) return res; else return defaultValue;
}

float CSTRCALL cstr::to_float(void) const {
	float res = 0.0;
	scanf("%f", &res);
	return res;
}

float CSTRCALL cstr::to_float(float defaultValue) const {
	float res = 0.0;
	if (scanf("%f", &res) == 1) return res; else return defaultValue;
}

double CSTRCALL cstr::to_double(void) const {
	double res = 0.0;
	scanf("%lf", &res);
	return res;
}

double CSTRCALL cstr::to_double(double defaultValue) const {
	double res = 0.0;
	if (scanf("%lf", &res) == 1) return res; else return defaultValue;
}

void CSTRCALL cstr::lowercase(void) {
	if (m_data == NULL || m_data->buf == NULL) return;
	m_fork_copy(m_data->l);
	for (unsigned int i = 0; i<m_data->l-1; i++)
	m_data->buf[i] = tolower(m_data->buf[i]);
}

void CSTRCALL cstr::uppercase(void) {
	if (m_data == NULL || m_data->buf == NULL) return;
	m_fork_copy(m_data->l);
	for (unsigned int i = 0; i<m_data->l-1; i++)
	m_data->buf[i] = toupper(m_data->buf[i]);
}

cstr CSTRCALL cstr::extract_file_ext(void) const {
	if (m_data == NULL || m_data->buf == NULL) return cstr();
	// char *last_dot = strrchr(m_data->buf, SEPARATOR_EXT), *last_slash = strrchr(m_data->buf, SEPARATOR_DIR);
	char *last_dot = m_strrchr(SEPARATOR_EXT), *last_slash = m_strrchr(SEPARATOR_DIR);
	if (last_dot == NULL || last_dot<last_slash) return cstr();
	return last_dot + 1;
}

cstr CSTRCALL cstr::extract_file_name(void) const {
	if (m_data == NULL || m_data->buf == NULL) return cstr();
	// char *last_dot = strrchr(m_data->buf, SEPARATOR_EXT), *last_slash = strrchr(m_data->buf, SEPARATOR_DIR);
	char *last_dot = m_strrchr(SEPARATOR_EXT), *last_slash = m_strrchr(SEPARATOR_DIR);
	if (last_dot == NULL&&last_slash == NULL) return *this;
	if (last_dot>last_slash) *last_dot = 0;
	cstr res;
	if (last_slash == NULL) res = m_data->buf; else res = last_slash + 1;
	if (last_dot>last_slash) *last_dot = SEPARATOR_EXT;
	return res;
}

cstr CSTRCALL cstr::extract_file_path(void) const {
	if (m_data == NULL || m_data->buf == NULL) return cstr();
	// char *last_slash = strrchr(m_data->buf, SEPARATOR_DIR);
	char *last_slash = m_strrchr(SEPARATOR_DIR);
	if (last_slash == NULL) return cstr();
	char bak=*(last_slash + 1);
	*(last_slash + 1) = 0;
	cstr res = m_data->buf;
	*(last_slash + 1) = bak;
	return res;
}

cstr CSTRCALL cstr::change_file_ext(const cstr& ext) const {
	if (m_data == NULL || m_data->buf == NULL) return ext;
	// char *last_dot = strrchr(m_data->buf, SEPARATOR_EXT), *last_slash = strrchr(m_data->buf, SEPARATOR_DIR);
	char *last_dot = m_strrchr(SEPARATOR_EXT), *last_slash = m_strrchr(SEPARATOR_DIR);
	if (last_dot == NULL&&last_slash == NULL) return *this + ext;
	if (last_dot>last_slash) *last_dot = 0;
	cstr res;
	res = cstr(m_data->buf) + ext;
	if (last_dot>last_slash) *last_dot = SEPARATOR_EXT;
	return res;
}

cstr CSTRCALL cstr::change_file_ext(const char* ext) const {
	if (m_data == NULL || m_data->buf == NULL) return ext;
	// char *last_dot = strrchr(m_data->buf, SEPARATOR_EXT), *last_slash = strrchr(m_data->buf, SEPARATOR_DIR);
	char *last_dot = m_strrchr(SEPARATOR_EXT), *last_slash = m_strrchr(SEPARATOR_DIR);
	if (last_dot == NULL&&last_slash == NULL) return *this + ext;
	if (last_dot>last_slash) *last_dot = 0;
	cstr res;
	res = cstr(m_data->buf) + ext;
	if (last_dot>last_slash) *last_dot = SEPARATOR_EXT;
	return res;
	// return extract_file_path() + extract_file_name() + ext;
}

char* CSTRCALL cstr::m_strrchr(const char chr) const {
	if (m_data == NULL || m_data->buf == NULL) return NULL;
	for (unsigned int i = m_data->l-1; i > 0; i--)
	if (m_data->buf[i] == chr) return m_data->buf + i;
	if (m_data->buf[0] == chr) return m_data->buf;
	return NULL;
}
