
#include "..\..\..\include\spa\tools\cthread.h"

DWORD WINAPI threadProc(void *_this)
{
	if (!_this)
		return -1;
	return ((CThread *)_this)->run();
}

SPACALL CThread::CThread()
{
	mBreakThread = false;
	mThread = NULL;
}

SPACALL CThread::~CThread()
{
	terminate(1000);
}

bool SPACALL CThread::start()
{
	mBreakThread = false;

	// Create the thread
	mThread = CreateThread(NULL, 0, threadProc, (void*) this, 0, NULL);
	return mThread != NULL;
}

void SPACALL CThread::suspend()
{
	if (mThread)
		SuspendThread(mThread);
}

void SPACALL CThread::resume()
{
	if (mThread)
		ResumeThread(mThread);
}

int SPACALL CThread::terminate(unsigned int timeoutms)
{
	if (!mThread)
		return -1;

	// Break
	mBreakThread = true;

	// Resume the thread if paused
	while ((int)ResumeThread(mThread) > 0) { }


	// Wait for the thread to finish...
	int wait = WaitForSingleObject(mThread, timeoutms);
	if (wait == WAIT_TIMEOUT)  {
		kill(); // ... or just kill it x(
		return -2;
	}

	// Close
	if (CloseHandle(mThread) == FALSE) {
		mThread = NULL;	
		return -1;
	}
	mThread = NULL;	

	return 0;
}

void SPACALL CThread::kill()
{
	if (mThread)  {
		TerminateThread(mThread, 0);
		mThread = NULL;
	}
}

