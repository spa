/*
 cstr extension implemetenting some string search algorithms
 */

#include "..\..\..\include\spa\tools\cstrsearch.h"
#include <string.h>

#define CSTRSEARCH_ALPHABET_SIZE 256
#ifndef MAX
#define MAX(a, b) (((a)>(b))?(a):(b))
#endif
#ifndef MIN
#define MIN(a, b) (((a)<(b))?(a):(b))
#endif

//finding methods
int CSTRCALL cstrsearch::find(const char* pattern, unsigned int startChar, int searchAlgorithm) const {
	if (pattern == NULL) return -1;
	unsigned int length = 0;
	while (pattern[length] != 0)
		length++;

	return find(pattern, length, startChar, searchAlgorithm);
}



int CSTRCALL cstrsearch::find(const cstr &pattern, unsigned int startChar, int searchAlgorithm) const {
	if (pattern.c_str() == NULL) return -1;
	return find(pattern.c_str(), pattern.length(), startChar, searchAlgorithm);
}



/*protected*/
int CSTRCALL cstrsearch::find(const char* pattern, unsigned int length, unsigned int startChar, int searchAlgorithm) const {
	if (m_data == NULL) return -1;
	if (startChar >= m_data->l - 1) return -1;
	if (length == 0 || length > m_data->l) return -1;
	int res;
	switch (searchAlgorithm) {
		case CSTRSEARCH_APOSTOLICO_CROCHEMORE:
			res = AXAMAC(pattern, length, m_data->buf + startChar, m_data->l - 1 - startChar, NULL, 0);
			break;
		case CSTRSEARCH_BOYER_MOORE_TURBO:
			res = TBM((unsigned char*)pattern, length, (unsigned char*) m_data->buf + startChar, m_data->l - 1 - startChar, NULL, 0);
			break;
		case CSTRSEARCH_NOT_SO_NAIVE:
			res = NSN(pattern, length, m_data->buf + startChar, m_data->l - 1 - startChar, NULL, 0);
			break;
		default:
			return -1;
	}
	if (res >= 0) res += startChar;
	return res;
}



//searching methods
int CSTRCALL cstrsearch::search(const char* pattern, unsigned int* matchedIndices, unsigned int maxOccurrences, unsigned int startChar, int searchAlgorithm) const {
	if (pattern == NULL) return 0;
	unsigned int length = 0;
	while (pattern[length] != 0)
		length++;

	return search(pattern, length, matchedIndices, maxOccurrences, startChar, searchAlgorithm);
}




int CSTRCALL cstrsearch::search(const cstr &pattern, unsigned int* matchedIndices, unsigned int maxOccurrences, unsigned int startChar, int searchAlgorithm) const {
	if (pattern.c_str() == NULL) return 0;
	return search(pattern.c_str(), pattern.length(), matchedIndices, maxOccurrences, startChar, searchAlgorithm);
}



/*protected*/
int CSTRCALL cstrsearch::search(const char* pattern, unsigned int length, unsigned int* matchedIndices, unsigned int maxOccurrences, unsigned int startChar, int searchAlgorithm) const {
	if (m_data == NULL) return 0;
	if (startChar >= m_data->l - 1) return 0;
	if (length == 0 || length > m_data->l) return 0;
	if (matchedIndices == NULL) return 0;
	int res;
	switch (searchAlgorithm) {
		case CSTRSEARCH_APOSTOLICO_CROCHEMORE:
			res = AXAMAC(pattern, length, m_data->buf + startChar, m_data->l - 1 - startChar, matchedIndices, maxOccurrences);
			break;
		case CSTRSEARCH_BOYER_MOORE_TURBO:
			res = TBM((unsigned char*)pattern, length, (unsigned char*)m_data->buf + startChar, m_data->l - 1 - startChar, matchedIndices, maxOccurrences);
			break;
		case CSTRSEARCH_NOT_SO_NAIVE:
			res = NSN(pattern, length, m_data->buf + startChar, m_data->l - 1 - startChar, matchedIndices, maxOccurrences);
			break;
		default:
			return 0;
	}
	for (int i = 0; i < res; i++) matchedIndices[i] += startChar;
	return res;
}




//searching algorithms

/*Apostolico-Crochemore*/
void CSTRCALL cstrsearch::preKmp(const char *x, unsigned int m, int kmpNext[]) const {
   int i, j;

   i = 0;
   j = kmpNext[0] = -1;
   while (i < m) {
      while (j > -1 && x[i] != x[j])
         j = kmpNext[j];
      i++;
      j++;
      if (x[i] == x[j])
         kmpNext[i] = kmpNext[j];
      else
         kmpNext[i] = j;
   }
}

int CSTRCALL cstrsearch::AXAMAC(const char *x, unsigned int m, const char *y, unsigned int n, unsigned int* matchedIndices, unsigned int maxMatchesCount) const {
   int* kmpNext = new int [m+1];
   int i, j, k, ell;
   int matchesCount = 0;

   /* Preprocessing */
   preKmp(x, m, kmpNext);
   for (ell = 1; x[ell - 1] == x[ell]; ell++);
   if (ell == m)
      ell = 0;

   /* Searching */
   i = ell;
   j = k = 0;
   while (j <= n - m) {
      while (i < m && x[i] == y[i + j])
         ++i;
      if (i >= m) {
         while (k < ell && x[k] == y[j + k])
            ++k;
		 if (k >= ell) {
		    //end if no more search is needed
			 if (matchedIndices == NULL) {
				 delete[] kmpNext;
				 return j;
			 }
			 else {
				if (matchesCount == 0 || matchedIndices[matchesCount-1] + m <= j) {
					matchedIndices[matchesCount++] = j;
					if (matchesCount == maxMatchesCount) {
						delete[] kmpNext;
						return matchesCount;
					}
				}
			 }
		 }
      }
      j += (i - kmpNext[i]);
      if (i == ell)
         k = MAX(0, k - 1);
      else
         if (kmpNext[i] <= ell) {
            k = MAX(0, kmpNext[i]);
            i = ell;
         }
         else {
            k = ell;
            i = kmpNext[i];
         }
   }
   delete[] kmpNext;
   return (matchedIndices == NULL)? -1 : matchesCount;
}


/*turbo boyer-moor*************************************************************/
void CSTRCALL cstrsearch::preBmBc(const unsigned char *x, unsigned int m, int bmBc[]) const {
   int i;
 
   for (i = 0; i < CSTRSEARCH_ALPHABET_SIZE; ++i)
      bmBc[i] = m;
   for (i = 0; i < m - 1; ++i)
      bmBc[x[i]] = m - i - 1;
}
 
 
void CSTRCALL cstrsearch::suffixes(const unsigned char *x, unsigned int m, int *suff) const {
   int f, g, i;
 
   suff[m - 1] = m;
   g = m - 1;
   for (i = m - 2; i >= 0; --i) {
      if (i > g && suff[i + m - 1 - f] < i - g)
         suff[i] = suff[i + m - 1 - f];
      else {
         if (i < g)
            g = i;
         f = i;
         while (g >= 0 && x[g] == x[g + m - 1 - f])
            --g;
         suff[i] = f - g;
      }
   }
}
 
void CSTRCALL cstrsearch::preBmGs(const unsigned char *x, unsigned int m, int bmGs[]) const {
   int i, j;
   int* suff = new int[m];

   suffixes(x, m, suff);
 
   for (i = 0; i < m; ++i)
      bmGs[i] = m;
   j = 0;
   for (i = m - 1; i >= 0; --i)
      if (suff[i] == i + 1)
         for (; j < m - 1 - i; ++j)
            if (bmGs[j] == m)
               bmGs[j] = m - 1 - i;
   for (i = 0; i <= m - 2; ++i)
      bmGs[m - 1 - suff[i]] = m - 1 - i;

   delete[] suff;
}


int CSTRCALL cstrsearch::TBM(const unsigned char *x, unsigned int m, const unsigned char *y, unsigned int n, unsigned int* matchedIndices, unsigned int maxMatchesCount) const {
	if (m == 1) {
		return naive((char*) x, m, (char*) y, n, matchedIndices, maxMatchesCount);
	}
   int bcShift, i, j, shift, u, v, turboShift, bmBc[CSTRSEARCH_ALPHABET_SIZE];
   int* bmGs = new int[m];
   int matchesCount = 0;

   /* Preprocessing */
   preBmGs(x, m, bmGs);
   preBmBc(x, m, bmBc);

   /* Searching */
   j = u = 0;
   shift = m;
   while (j <= n - m) {
      i = m - 1;
      while (i >= 0 && x[i] == y[i + j]) {
         --i;
         if (u != 0 && i == m - 1 - shift)
            i -= u;
      }
      if (i < 0) {
         //end if no more search is needed
		  if (matchedIndices == NULL) {
			  delete[] bmGs;
			  return j;
		  }
		 else {
			if (matchesCount == 0 || matchedIndices[matchesCount-1] + m <= j) {
				matchedIndices[matchesCount++] = j;
				if (matchesCount == maxMatchesCount) {
					delete[] bmGs;
					return matchesCount;
				}
			}
		 }

         shift = bmGs[0];
         u = m - shift;
      }
      else {
         v = m - 1 - i;
         turboShift = u - v;
         bcShift = bmBc[y[i + j]] - m + 1 + i;
         shift = MAX(turboShift, bcShift);
         shift = MAX(shift, bmGs[i]);
         if (shift == bmGs[i])
            u = MIN(m - shift, v);
         else {
           if (turboShift < bcShift)
              shift = MAX(shift, u + 1);
           u = 0;
         }
      }
      j += shift;
   }
   delete[] bmGs;
   return (matchedIndices == NULL)? -1 : matchesCount;
}

//"Not so naive" algorithm
int CSTRCALL cstrsearch::NSN(const char *x, unsigned int m, const char *y, unsigned int n, unsigned int* matchedIndices, unsigned int maxMatchesCount) const{
	//when searching string of 1 use naive algorithm
	if (m == 1) {
		return naive(x, m, y, n, matchedIndices, maxMatchesCount);
	}

	int j, k, ell;
	int matchesCount = 0;
	
   /* Preprocessing */
   if (x[0] == x[1]) {
      k = 2;
      ell = 1;
   }
   else {
      k = 1;
      ell = 2;
   }
  
   /* Searching */
   j = 0;
   while (j <= n - m)
      if (x[1] != y[j + 1])
         j += k;
      else {
         if (memcmp(x + 2, y + j + 2, m - 2) == 0 &&
			 x[0] == y[j]) {
            //end if no more search is needed
			 if (matchedIndices == NULL) {
				  return j;
			 }
			 else {
				if (matchesCount == 0 || matchedIndices[matchesCount-1] + m <= j) {
					matchedIndices[matchesCount++] = j;
					if (matchesCount == maxMatchesCount) {
						return matchesCount;
					}
				}
			 }
		 }
         j += ell;
      }
  return (matchedIndices == NULL)? -1 : matchesCount;
}

//very naive algorithm used for m = 1
int CSTRCALL cstrsearch::naive(const char *x, unsigned int m, const char *y, unsigned int n, unsigned int* matchedIndices, unsigned int maxMatchesCount) const {
	//return -1;
	int j = 0;
	int matchesCount = 0;
	while (j <= n - m) {
		if (x[0] == y[j]) {
			//end if no more search is needed
			if (matchedIndices == NULL) {
				  return j;
			}
			else {
				if (matchesCount == 0 || matchedIndices[matchesCount-1] + m <= j) {
					matchedIndices[matchesCount++] = j;
					if (matchesCount == maxMatchesCount) return matchesCount;
				}
			 }
		}
		j++;
	}
	return (matchedIndices == NULL) ? -1 : matchesCount;
}




//replacing



/*protected*/
cstr CSTRCALL cstrsearch::replace(unsigned int* matchedIndices, unsigned int numIndices, unsigned int patternLength, const char* text, unsigned int textLength) {
	cstrsearch old = *this;
	m_set_length(m_data->l + numIndices * (textLength - patternLength));
	if (m_data == NULL || m_data->buf == NULL) {
		*this = old;
		return cstr();
	}
	unsigned int currentPosDest = 0, currentPosSrc = 0;
	for (unsigned int i = 0; i < numIndices; i++) {
		memcpy(m_data->buf + currentPosDest, old.m_data->buf + currentPosSrc, matchedIndices[i] - currentPosSrc);
		currentPosDest += matchedIndices[i] - currentPosSrc;
		if (text != NULL) memcpy(m_data->buf + currentPosDest, text, textLength);
		currentPosDest += textLength;
		currentPosSrc = matchedIndices[i] + patternLength;
	}
	memcpy(m_data->buf + currentPosDest, old.m_data->buf + currentPosSrc, old.length() - currentPosSrc);
	cstr result = *this;
	*this = old;
	return result;
}




/*protected*/
cstr CSTRCALL cstrsearch::replace(const char* pattern, unsigned int patternLength, const char* text, unsigned int textLength, int searchAlgorithm) {
	unsigned int maxIndices = m_data->l / patternLength, *matchedIndices = new unsigned int [maxIndices];
	unsigned int numIndices = search(pattern, patternLength, matchedIndices, maxIndices, 0, searchAlgorithm);
	if (numIndices < 1) return *this;
	cstr result = replace(matchedIndices, numIndices, patternLength, text, textLength);
	delete[] matchedIndices;
	return result;
}




/*protected*/
cstr CSTRCALL cstrsearch::replaceOnce(const char* pattern, unsigned int patternLength, const char* text, unsigned int textLength, unsigned int startChar, int searchAlgorithm) {
	unsigned int matchedIndices;
	unsigned int numIndices = search(pattern, patternLength, &matchedIndices, 1, startChar, searchAlgorithm);
	if (numIndices < 1) return *this;
	return replace(&matchedIndices, numIndices, patternLength, text, textLength);
	
}






cstr CSTRCALL cstrsearch::replace(const char* pattern, const char* text, int searchAlgorithm) {
	if (m_data == NULL || m_data->buf == NULL) return *this;
	if (pattern == NULL) return *this;
	unsigned int patternLength = 0;
	while (pattern[patternLength]) patternLength++;
	unsigned int textLength = 0;
	if (text != NULL) while (text[textLength]) textLength++;
	if (patternLength >= m_data->l) return *this;
	return replace(pattern, patternLength, text, textLength, searchAlgorithm);
}




cstr CSTRCALL cstrsearch::replace(const cstr& pattern, const cstr& text, int searchAlgorithm) {
	if (m_data == NULL || m_data->buf == NULL) return *this;
	if (pattern.c_str() == NULL) return *this;
	if (pattern.length() >= m_data->l) return *this;
	return replace(pattern.c_str(), pattern.length(), text.c_str(), text.length(), searchAlgorithm);
}



cstr CSTRCALL cstrsearch::replaceOnce(const char* pattern, const char* text, unsigned int startChar, int searchAlgorithm) {
	if (m_data == NULL || m_data->buf == NULL) return *this;
	if (pattern == NULL) return *this;
	unsigned int patternLength = 0;
	while (pattern[patternLength]) patternLength++;
	unsigned int textLength = 0;
	if (text != NULL) while (text[textLength]) textLength++;
	if (patternLength >= m_data->l) return *this;
	return replaceOnce(pattern, patternLength, text, textLength, startChar, searchAlgorithm);
}



cstr CSTRCALL cstrsearch::replaceOnce(const cstr& pattern, const cstr& text, unsigned int startChar, int searchAlgorithm) {
	if (m_data == NULL || m_data->buf == NULL) return *this;
	if (pattern.c_str() == NULL) return *this;
	if (pattern.length() >= m_data->l) return *this;
	return replaceOnce(pattern.c_str(), pattern.length(), text.c_str(), text.length(), startChar, searchAlgorithm);
}
