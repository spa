/*
 cstr extension to accomodate struct GUID conversion to registry format string and vice versa

 */

#include "..\..\..\include\spa\tools\cstrguid.h"

CSTRCALL cstrguid::cstrguid(const GUID &guid) {
	printf("{%08lX-%04hX-%04hX-%02hX%02hX-%02hX%02hX%02hX%02hX%02hX%02hX}", guid.Data1, guid.Data2, guid.Data3,
			guid.Data4[0], guid.Data4[1], guid.Data4[2], guid.Data4[3], guid.Data4[4], guid.Data4[5], guid.Data4[6],
			guid.Data4[7]);
}

int CSTRCALL cstrguid::to_GUID(GUID &guid) const {
	int data[11];
	int argRead = scanf("{%8x-%4x-%4x-%2x%2x-%2x%2x%2x%2x%2x%2x}", &data[0], &data[1], &data[2], &data[3], &data[4], &data[5], &data[6], &data[7], &data[8], &data[9], &data[10]);
	if (argRead != 11) return 0;
	guid.Data1 = data[0];
	guid.Data2 = data[1];
	guid.Data3 = data[2];
	for (int i = 0; i<8; i++) guid.Data4[i] = data[3 + i];
	return 1;
}
