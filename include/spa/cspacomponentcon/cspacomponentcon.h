/*
 Simple Plugin Architecture
 aut: jv
 est: 060612

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef CSPACOMPONENTCON_H
#define CSPACOMPONENTCON_H

#include "..\cspacomponent\cspacomponent.h"
#include "..\interfaces\ispaconsole.h"
#include "..\tools\ccrisec.h"

class CSPAComponentCon: virtual public CSPAComponent {
protected:
	ISPAConsole *mCon;
	CCrisec *mCSCon;
	virtual int SPACALL logInfo(char* msg) {
		if (mCon != NULL) return mCon->message(mName.c_str(), msg, SPACONMSG_INFO, 0); else return -1;
	}
	virtual int SPACALL logWarn(char* msg) {
		if (mCon != NULL) return mCon->message(mName.c_str(), msg, SPACONMSG_WARN, 0); else return -1;
	}
	virtual int SPACALL logError(char* msg) {
		if (mCon != NULL) return mCon->message(mName.c_str(), msg, SPACONMSG_ERROR, 0); else return -1;
	}
	virtual int SPACALL logFatal(char* msg) {
		if (mCon != NULL) return mCon->message(mName.c_str(), msg, SPACONMSG_FATAL, 0); else return -1;
	}
	/*virtual int SPACALL mpinfo(char* msg) {
		if (mCon != NULL) return mCon->message(mName.c_str(), msg, SPACONMSG_INFO, SPACONMSGFLG_POP); else return -1;
	}
	virtual int SPACALL mpwarn(char* msg) {
		if (mCon != NULL) return mCon->message(mName.c_str(), msg, SPACONMSG_WARN, SPACONMSGFLG_POP); else return -1;
	}
	virtual int SPACALL mperror(char* msg) {
		if (mCon != NULL) return mCon->message(mName.c_str(), msg, SPACONMSG_ERROR, SPACONMSGFLG_POP); else return -1;
	}
	virtual int SPACALL mpfatal(char* msg) {
		if (mCon != NULL) return mCon->message(mName.c_str(), msg, SPACONMSG_FATAL, SPACONMSGFLG_POP); else return -1;
	}*/

	SPACMDDECL(CSPAComponentCon, con);
public:
	SPACALL CSPAComponentCon();
	virtual SPACALL ~CSPAComponentCon() {SDELETE(mCSCon)}
	/* ISPAComponent */
	STDMETHOD_(void, finish)();
};

#endif
