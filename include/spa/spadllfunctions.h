
/*
Simple Plugin Architecture
aut: jv
est: 060711

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

*/


#ifndef SPADLLFUNCTIONS_H
#define SPADLLFUNCTIONS_H

#define __SPADLL_CALL    _stdcall

#define GETCOMPONENT_RESULT_OK            1
#define GETCOMPONENT_RESULT_NOTFOUND      2
#define GETCOMPONENT_RESULT_PARAMERR      3
#define GETCOMPONENT_RESULT_INTERNALERR   4

#define __SPADLL_GETCOMPONENT_TYPE        int
#define __SPADLL_GETCOMPONENT_NAME        getComponent
#define __SPADLL_GETCOMPONENT_NAME_       __SPADLL_CALL __SPADLL_GETCOMPONENT_NAME
#define __SPADLL_GETCOMPONENT_ATTR        (ISPAComponent** result, char* component, char* name,ISPAManager* man)


#define __SPADLL_GETCOMPONENTLIST_TYPE    char*
#define __SPADLL_GETCOMPONENTLIST_NAME    getComponentList
#define __SPADLL_GETCOMPONENTLIST_NAME_   __SPADLL_CALL __SPADLL_GETCOMPONENTLIST_NAME
#define __SPADLL_GETCOMPONENTLIST_ATTR    ()


#define __SPADLL_GETVERSION_TYPE          int 
#define __SPADLL_GETVERSION_NAME          getVersion
#define __SPADLL_GETVERSION_NAME_         __SPADLL_CALL __SPADLL_GETVERSION_NAME
#define __SPADLL_GETVERSION_ATTR          ()


#define __SPADLL_RELEASE_TYPE             int
#define __SPADLL_RELEASE_NAME             release
#define __SPADLL_RELEASE_NAME_            __SPADLL_CALL __SPADLL_RELEASE_NAME
#define __SPADLL_RELEASE_ATTR             ()


#define __SPADLL_GETCOMPONENT               __SPADLL_GETCOMPONENT_TYPE     __SPADLL_GETCOMPONENT_NAME_     __SPADLL_GETCOMPONENT_ATTR 
#define __SPADLL_GETCOMPONENTLIST           __SPADLL_GETCOMPONENTLIST_TYPE __SPADLL_GETCOMPONENTLIST_NAME_ __SPADLL_GETCOMPONENTLIST_ATTR 
#define __SPADLL_GETVERSION                 __SPADLL_GETVERSION_TYPE       __SPADLL_GETVERSION_NAME_       __SPADLL_GETVERSION_ATTR
#define __SPADLL_RELEASE                    __SPADLL_RELEASE_TYPE          __SPADLL_RELEASE_NAME_          __SPADLL_RELEASE_ATTR


#define SPADLL_GETCOMPONENT \
extern "C" __declspec(dllexport) __SPADLL_GETCOMPONENT;\
__SPADLL_GETCOMPONENT

#define SPADLL_GETCOMPONENTLIST \
extern "C" __declspec(dllexport) __SPADLL_GETCOMPONENTLIST;\
__SPADLL_GETCOMPONENTLIST

#define SPADLL_GETVERSION \
extern "C" __declspec(dllexport) __SPADLL_GETVERSION;\
__SPADLL_GETVERSION

#define SPADLL_RELEASE \
extern "C" __declspec(dllexport) __SPADLL_RELEASE;\
__SPADLL_RELEASE


#endif
