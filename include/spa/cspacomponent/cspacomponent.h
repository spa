/*
 Simple Plugin Architecture
 aut: jv
 est: 060609

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef CSPACOMPONENT_H
#define CSPACOMPONENT_H

#include "../base/spabase.h"
#include "../interfaces/ispacomponent.h"
#include "../interfaces/ispamanager.h"
#include "../tools/cpcharhashmap.h"
#include "../tools/ccrisec.h"
#include "../tools/cstr.h"
#include "../spaaux/clog.h"

/* command function declaration header */
/* can be used to override command method */
#define SPACMDHD(cmdName) virtual char* SPACALL cmd_##cmdName(TSPACMD* cmd)

/* command function declaration */
#define SPACMDDECL(cls,cmdName)\
	class CCmd_##cmdName:public mCCall{\
	protected:\
		cls *o;\
	public:\
		SPACALL CCmd_##cmdName(char* help,cls *a):mCCall(help){ o = a; }\
		virtual char* SPACALL call(TSPACMD* cmd){ return o->cmd_##cmdName(cmd); }\
	};\
	friend class CCmd_##cmdName;\
	SPACMDHD(cmdName)

/* command function definition header */
#define SPACMDDEF(cls,cmdName) char* SPACALL cls::cmd_##cmdName(TSPACMD* cmd)

/* command function call */
#define SPACMDCALL(cmdName,par) cmd_##cmdName(par)

/* inherited command function call */
#define SPACMDINHERITED(cls,cmdName,par) cls::cmd_##cmdName(par)

/* command registration */
#define SPACMDREG_(cmdName,cmdtxt,cmdhlp) mCmdMap->insert(mTCMDMAPVAL(cmdtxt,new CCmd_##cmdName(cmdhlp,this)));
#define SPACMDREG(cmd,cmdhlp) SPACMDREG_(cmd,#cmd,cmdhlp)

/* default command registration */
#define SPACMDREGDEFAULT_(cmdName,cmdtxt,cmdhlp) \
	if (mDefaultCmd == NULL){ \
		mDefaultCmdID = cmdtxt; \
		mDefaultCmd = new CCmd_##cmdName(cmdhlp,this); \
		mCmdMap->insert(mTCMDMAPVAL(cmdtxt,mDefaultCmd)); \
	}
	
#define SPACMDREGDEFAULT(cmd,cmdhlp) SPACMDREGDEFAULT_(cmd,#cmd,cmdhlp)


/* common call results */
extern char* sok;
extern char* serr;

#if(SPALOGLVL>SPALOGLVL_DETAIL)
#define SOK mMan->newStr(sok)
#define SERR mMan->newStr(serr)
#else
#define SOK NULL
#define SERR mMan->newStr(serr)
#endif


/* helper macro to fill mAbout literal constant
 * SPAPACKAGE_VERSION is expected to be defined and contain "major.minor.yymmdd"
 * best to define using IDE project options 
 *  Visual Studio:
 *  Project -> Properties -> Configuration Properties -> C/C++ -> Preprocessor
 *  Release: Preprocessor Definitions: ...;SPAPACKAGE_VERSION=\"major.minor.yymmdd\";...  
 *  Debug:   Preprocessor Definitions: ...;SPAPACKAGE_VERSION=\"major.minor.yymmdd debug\";...
 *  (note the escape sequence)   
 */
#define SPACOMPONENT_ABOUT(component, author, about) \
mAbout = component"\r\nAuthor(s): "author"\r\nVersion: "SPAPACKAGE_VERSION"\r\n"about;



class CSPAComponent: public ISPAComponent, virtual public CLog {
protected:
	class mCCall {
	public:
		char *mHelp;
		inline SPACALL mCCall(char* help) {
			mHelp = help;
		}
		virtual char* SPACALL call(TSPACMD* cmd) = 0;
	};
	typedef CPCharHashMap<mCCall*> mTCMDMAP;
	typedef mTCMDMAP::iterator mTCMDMAPITER;
	typedef mTCMDMAP::value_type mTCMDMAPVAL;
	mTCMDMAP *mCmdMap;
	char *mDefaultCmdID,*mAbout;
	mCCall *mDefaultCmd;
	ISPAManager *mMan;
	cstr mName;
	int mRefCount;
	CCrisec *mCSRef;

	SPACMDDECL(CSPAComponent, about);
	SPACMDDECL(CSPAComponent, help);
	//  SPACMDDECL(CSPAComponent, cmdList);
public:
	SPACALL CSPAComponent();
	virtual SPACALL ~CSPAComponent();
	/* IUnknown */
	STDMETHOD( QueryInterface)(REFIID riid, void** ppvObj);
	STDMETHOD_(ULONG, AddRef)();
	STDMETHOD_(ULONG, Release)();
	/* ISPAComponent */
	STDMETHOD_(char*, call)(TSPACMD* cmd);
	STDMETHOD_(char*, name)() {return mName.c_str();}
};

#endif
