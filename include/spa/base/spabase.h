/*
 Simple Plugin Architecture
 aut: jv
 est: 060426
 
    This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef SPABASE_H
#define SPABASE_H

/* SPA version */
#define SPAVERSION 0x00000002L
#define SPAVERSIONCORRECT(version) ((version) == SPAVERSION)

/* SPA call type */
#define SPACALL __fastcall

/* SPA logging levels */
#define SPALOGLVL_NONE   0
#define SPALOGLVL_INFO   64
#define SPALOGLVL_DETAIL 96
#define SPALOGLVL_DEBUG  128

#if defined _DEBUG
#define SPALOGLVL SPALOGLVL_DETAIL
#else
#define SPALOGLVL SPALOGLVL_INFO
#endif

/* safe delete and release */
#define SDELETE(ptr)  if ((ptr) != NULL){ delete (ptr); (ptr) = NULL; }
#define SDELETE_(ptr) if ((ptr) != NULL){ delete[] (ptr); (ptr) = NULL; }
#define SRELEASE(ptr) if ((ptr) != NULL){ (ptr)->Release(); (ptr) = NULL; }

/* NULL if required */
#ifndef NULL
#define NULL 0L
#endif

/* macro to convert macro to string */
#define __TOSTR_(attr) #attr
#define __TOSTR(attr) __TOSTR_(attr)

#endif
