/*
 Simple Plugin Architecture
 aut: jv
 est: 080625

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef CCSTRHASHMAP_H
#define CCSTRHASHMAP_H

#include <hash_map>

#include "cstr.h"


#ifdef _STLPORT_VERSION
using namespace std;

class CcstrHashMap_hash_compare {
public:
	bool operator () (const cstr& a, const cstr& b) const  { return a < b; }
};

template<> class hash<cstr>  {
	size_t operator( )(const cstr& s) const {
		unsigned long h = 5381;
		const int l = s.length();
		for (register int i = 0; i < l; i++)
			h += (h << 5) + s[i];
		return size_t(h);
	}	
};

template<class T> class CcstrHashMap: public hash_map<const cstr, T, hash<cstr>, CcstrHashMap_hash_compare> {
};

#else

using namespace stdext;

class CcstrHashMap_hash_compare {
public:
	enum {
		bucket_size = 4, min_buckets = 8
	};
	CcstrHashMap_hash_compare() {
	}

	size_t operator( )(const cstr& s) const {
		unsigned long h = 5381;
		const int l = s.length();
		for (register int i = 0; i < l; i++)
			h += (h << 5) + s[i];
		return size_t(h);
	}	

	bool operator( )(const cstr& s1, const cstr& s2) const {
		return s1 < s2;
	}
};

template<class T> class CcstrHashMap: public hash_map<const cstr, T, CcstrHashMap_hash_compare> {
};
#endif

#endif
