/*
 Simple Plugin Architecture
 aut: jv
 est: 070115

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef CWAITABLE_H
#define CWAITABLE_H

#include <windows.h>
#include "..\base\spabase.h"

/* wait results */
#define WR_OK        0
#define WR_TIMEOUT   1
#define WR_ABANDONED 2
#define WR_FAILED    3

/* infinite wait time */
#define W_INF INFINITE

class CWaitable {
protected:
	HANDLE mHandle;
public:
	SPACALL CWaitable() {
		mHandle = NULL;
	}
	virtual SPACALL ~CWaitable() {
	}
	int SPACALL wait(unsigned int time);
	/* Waits for several CWaitable* objects, the object responsible for the return is identified by its index returned in count */  
	static int waitMultiple(unsigned int time, unsigned int* count, bool waitAll, ...);
};

#endif
