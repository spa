/*
 cstr extension to accomodate struct GUID conversion to registry format string and vice versa

 */

#ifndef CSTRGUID_H
#define CSTRGUID_H

#define INITGUID
#define __INLINE_ISEQUAL_GUID
#include <objbase.h>

#include "cstr.h"

class cstrguid: public cstr {
public:
	/* Implicit constructor */
	CSTRCALL cstrguid() : cstr() { }

	/* Creates new instance converting from GUID structure */
	CSTRCALL cstrguid(const GUID &guid);

	/* Creates new instance from cstr */
	CSTRCALL cstrguid(const cstr &str) :
		cstr(str) {
	}

	/* Copy constructor */
	CSTRCALL cstrguid(const cstrguid& str) : cstr(*(cstr*)&str) { }

	/* Assignement operators from cstr */
	using cstr::operator =;

	/* Typecast operator to cstr */
	operator cstr() {
		return *(cstr*) this;
	}

	/* Converts registry formated string to an existing guid structure. Returns 0 in case of incorrect string format, 1 if success. */
	int CSTRCALL to_GUID(GUID &guid) const;
};

#endif
