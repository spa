/*
 Simple Plugin Architecture
 aut: jv
 est: 060605

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef CCRISEC_H
#define CCRISEC_H

//#define _WIN32_WINNT 0x0502
#include <windows.h>
#include "..\base\spabase.h"

#define CSSPINC 100 //spin count for simple testing of different values
class CCrisec {
protected:
	CRITICAL_SECTION mCriticalSection;
public:
	SPACALL CCrisec() {
		InitializeCriticalSection(&mCriticalSection);
	}
	SPACALL CCrisec(unsigned int aspinCount) {
		InitializeCriticalSectionAndSpinCount(&mCriticalSection, aspinCount);
	}
	virtual SPACALL ~CCrisec() {
		DeleteCriticalSection(&mCriticalSection);
	}
	void SPACALL enter() {EnterCriticalSection(&mCriticalSection);}
	void SPACALL leave() {LeaveCriticalSection(&mCriticalSection);}
	int SPACALL tryEnter() {return TryEnterCriticalSection(&mCriticalSection);}
};

#endif
