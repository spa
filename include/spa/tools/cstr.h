/*
	String wrapper, char* only, copy-on-write implementation,
	hopefully portable, further development intended (more utility functions etc.)
	
	Version 2.0
	
	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.


	Copyright (C) 2002 - 2005 Jan Vanek (honza.vanek@gmail.com)
	
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
	or visit http://www.gnu.org/copyleft/gpl.html
	
	Portions Copyright (C) 2001 by raf <raf@raf.org> (libslack)
*/

#ifndef CSTR_H
#define CSTR_H

#include <stdarg.h>

#ifndef NULL
#define NULL 0
#endif

#define CSTRCALL __fastcall

#define MINSIZE 8

#define SEPARATOR_EXT '.'
#define SEPARATOR_DIR '\\'

class cstr {
protected:
	typedef struct {
		char* buf;
		unsigned int l, alloc, ref;
	} TSHARED;
	TSHARED* m_data;
	inline void CSTRCALL m_fork(void);
	inline void CSTRCALL m_fork_copy(int length);
	inline void CSTRCALL m_set_length(int length);
	inline void CSTRCALL m_destroy_data(void);
	inline char* CSTRCALL m_strrchr(const char chr) const;
public:
	CSTRCALL cstr():m_data(NULL) {}
	CSTRCALL cstr(const cstr& str) {if (str.m_data != NULL) str.m_data->ref++; m_data = str.m_data;}
	CSTRCALL cstr(const char* str);

	CSTRCALL cstr(const char src):m_data(NULL) {printf("%c", src);}
	CSTRCALL cstr(const short src):m_data(NULL) {printf("%hd", src);}
	CSTRCALL cstr(const unsigned short src):m_data(NULL) {printf("%hu", src);}
	CSTRCALL cstr(const int src):m_data(NULL) {printf("%i", src);}
	CSTRCALL cstr(const unsigned int src):m_data(NULL) {printf("%u", src);}
	CSTRCALL cstr(const long src):m_data(NULL) {printf("%ld", src);}
	CSTRCALL cstr(const unsigned long src):m_data(NULL) {printf("%lu", src);}
	CSTRCALL cstr(const float src):m_data(NULL) {printf("%.15g", src);}
	CSTRCALL cstr(const double &src):m_data(NULL) {printf("%.15lg", src);}
	CSTRCALL cstr(const long double &src):m_data(NULL) {printf("%.15Lg", src);}

	virtual CSTRCALL ~cstr();

	cstr& CSTRCALL operator= (const cstr& rhs);

	cstr& CSTRCALL operator+= (const cstr rhs);
	cstr& CSTRCALL operator+= (const char* rhs);
	cstr& CSTRCALL operator+= (char rhs);
	cstr& CSTRCALL operator+= (const short rhs)  { return operator+=(cstr(rhs)); }
	cstr& CSTRCALL operator+= (const unsigned short rhs)  { return operator+=(cstr(rhs)); }
	cstr& CSTRCALL operator+= (const int rhs)  { return operator+=(cstr(rhs)); }
	cstr& CSTRCALL operator+= (const unsigned int rhs)  { return operator+=(cstr(rhs)); }
	cstr& CSTRCALL operator+= (const long rhs)  { return operator+=(cstr(rhs)); }
	cstr& CSTRCALL operator+= (const unsigned long rhs)  { return operator+=(cstr(rhs)); }
	cstr& CSTRCALL operator+= (const float rhs)  { return operator+=(cstr(rhs)); }
	cstr& CSTRCALL operator+= (const double rhs)  { return operator+=(cstr(rhs)); }
	cstr& CSTRCALL operator+= (const long double rhs)  { return operator+=(cstr(rhs)); }

	cstr CSTRCALL operator+ (const cstr& rhs) const;
	cstr CSTRCALL operator+ (const char* rhs) const;

	bool CSTRCALL operator== (const cstr& rhs) const;
	bool CSTRCALL operator!= (const cstr& rhs) const;
	bool CSTRCALL operator< (const cstr& rhs) const;
	bool CSTRCALL operator> (const cstr& rhs) const;
	bool CSTRCALL operator<= (const cstr& rhs) const;
	bool CSTRCALL operator>= (const cstr& rhs) const;

	bool CSTRCALL operator== (const char* rhs) const;
	bool CSTRCALL operator!= (const char* rhs) const;
	bool CSTRCALL operator< (const char* rhs) const;
	bool CSTRCALL operator> (const char* rhs) const;
	bool CSTRCALL operator<= (const char* rhs) const;
	bool CSTRCALL operator>= (const char* rhs) const;

	char CSTRCALL operator[](const unsigned int i) const {if (m_data == NULL || m_data->buf == NULL || i >= m_data->l) return 0; else return m_data->buf[i];}
	cstr CSTRCALL operator()(const unsigned int from, const unsigned int to) const;

	static cstr sprintf(const char* format, ...);
	int printf(const char* format, ...);
	int CSTRCALL vprintf(const char* format, va_list);
	int scanf(const char* format, ...) const;

	int CSTRCALL to_int(void) const;
	int CSTRCALL to_int(int defaultValue) const;
	float CSTRCALL to_float(void) const;
	float CSTRCALL to_float(float defaultValue) const;
	double CSTRCALL to_double(void) const;
	double CSTRCALL to_double(double defaultValue) const;

	void CSTRCALL lowercase(void);
	void CSTRCALL uppercase(void);

	cstr CSTRCALL extract_file_path(void) const;
	cstr CSTRCALL extract_file_ext(void) const;
	cstr CSTRCALL extract_file_name(void) const;
	cstr CSTRCALL change_file_ext(const cstr& ext) const;
	cstr CSTRCALL change_file_ext(const char* ext) const;

	unsigned int CSTRCALL length(void) const {if (m_data != NULL && m_data->l > 0) return m_data->l - 1; else return 0;}
	char* CSTRCALL c_str(void) const {if (m_data != NULL) return m_data->buf; else return "";}
};

#endif
