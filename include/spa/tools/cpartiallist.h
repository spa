
/**
 * Partial list class implementation.
 * A partial list is a standard linked list that is designed for working
 * in multithreaded environments. A typical use case of this class is
 * one thread reading and processing the items in the list while another
 * one keeps adding items to it. 
 * The list contains iterators that behave differently than the iterators
 * known from std::list or std::vector. The forward iterator of partial
 * list never equals end() until the list is marked as closed and the 
 * iterator ++ operator always waits (blocks) if there are no further
 * items in the list and the list has not been closed.
 * This special behavior allows creating two threads where one keeps
 * pushing new items to the list and the other one processes these
 * items as they come, waiting for new items that have not been pushed
 * to the list yet. After the pushing thread finishes its work it can
 * mark the list as closed and the processing thread will eventually break.
 */

#ifndef CPARTIALLIST_H
#define CPARTIALLIST_H

#include <list>
#include "ccrisec.h"
#include "cevent.h"

#include <assert.h>

/**
 * Iterator for the list
 */
template <class ListType, typename ItemType>
class _PListIterator  {
private:
	friend ListType;
	template <class U, typename V> friend class _PListIterator;

	typedef typename std::list<typename ItemType>::iterator STLListIter;

	ListType *mParent;
	STLListIter mIter;

private:
	_PListIterator(ListType *prn, const STLListIter& it) : mParent(prn), mIter(it)  {}

public:

	/**
	 * Copy constructor
	 */
	_PListIterator(const _PListIterator<ListType, ItemType>& oth) 
	{
		oth.mParent->mCSList->enter();
		mParent = oth.mParent;
		mIter = oth.mIter;
		oth.mParent->mCSList->leave();
	}

	template <class T> _PListIterator(T& oth)
	{
		oth.mParent->mCSList->enter();
		mParent = oth.mParent;
		mIter = oth.mIter;
		oth.mParent->mCSList->leave();
	}

	/**
	 * Copy operator
	 */
	_PListIterator& operator= (const _PListIterator& oth)
	{
		assign(oth);
		return *this;
	}

	template <class T>
	_PListIterator& operator= (const T& oth)
	{
		assign(oth);
		return *this;
	}

	/**
	 * Assignment (same as copy operator)
	 */
	template <class T>
	void assign(const T& oth)
	{
		if (&oth != this)  {
			ListType *oldPrn = mParent;
			oldPrn->mCSList->enter();
			oth.mParent->mCSList->enter();
			mParent = oth.mParent;
			mIter = oth.mIter;
			oth.mParent->mCSList->leave();
			oldPrn->mCSList->leave();
		}
	}

	/**
	 * Preincrement operator++
	 */
	_PListIterator& operator++()
	{
		mParent->mCSList->enter();
		assert(mIter != mParent->mInternal.end());
		STLListIter it = mIter;
		++it;

		// If this is the last iterator and the parent is not closed, wait
		if (it == mParent->mInternal.end() && !mParent->mClosed)  {
			size_t before = mParent->mInternal.size();
			mParent->mAddEvent->reset();
			mParent->mCSList->leave();
			CWaitable::waitMultiple(W_INF, 2, false, mParent->mAddEvent, mParent->mClosedEvent);
			mParent->mCSList->enter();
			//it = mIter; ++it;
			size_t after = mParent->mInternal.size();
			assert(before != after || mParent->mClosed);
			mParent->mAddEvent->reset();
			mParent->mClosedEvent->reset();
		}

		// Move on
		++mIter;

		mParent->mCSList->leave();

		return *this;
	}

	/**
	 * Postincrement operator++
	 */
	_PListIterator operator++(int)
	{
		_PListIterator res = *this;
		++*this;
		return res;
	}

	/**
	 * Preincrement operator--
	 */
	_PListIterator& operator--()
	{
		mParent->mCSList->enter();
		--mIter;
		mParent->mCSList->leave();

		return *this;
	}

	/**
	 * Postincrement operator--
	 */
	_PListIterator operator--(int)
	{
		_PListIterator res = *this;
		--*this;
		return res;
	}

	/**
	 * Dereference operator
	 */
	ItemType& operator *() const
	{
		mParent->mCSList->enter();
		ItemType& res = *mIter;
		mParent->mCSList->leave();
		return res;
	}

	/**
	 * Pointer to the object
	 */
	ItemType *operator ->() const
	{
		mParent->mCSList->enter();
		ItemType *res = &(*mIter);
		mParent->mCSList->leave();
		return res;
	}

	/**
	 * Comparison operator
	 */
	template <typename T>
	bool operator== (const T& oth)
	{
		mParent->mCSList->enter();
		const bool res = mIter == oth.mIter;
		mParent->mCSList->leave();
		return res;
	}

	/**
	 * Comparison operator
	 */
	template <typename T>
	bool operator!= (const T& oth)
	{
		return !(*this == oth);
	}

};

template <typename T>
class CPartialList  {
public:
	typedef _PListIterator<CPartialList<T>, T> iterator;
	typedef const iterator const_iterator;

private:
	friend iterator;
	std::list<T> mInternal;
	CEvent *mAddEvent;
	CEvent *mClosedEvent;
	CCrisec *mCSList;
	volatile bool mClosed;

public:
	/**
	 * Constructor - creates a closed list (the end can be reached using iterators).
	 */
	CPartialList()
	{
		// Start with the list closed, so that nobody shoots himself in the foot by starting to iterate over this list
		mClosed = true;
		mAddEvent = new CEvent();
		mClosedEvent = new CEvent();
		mAddEvent->reset();
		mClosedEvent->reset();
		mCSList = new CCrisec();
	}

	/**
	 * Destructor
	 */
	~CPartialList()
	{
		mCSList->enter();
		delete mAddEvent;
		mCSList->leave();
		delete mCSList;
	}

	/**
	 * Adds an item to the list
	 */
	void pushBack(const T& elem)
	{
		mCSList->enter();
		mInternal.push_back(elem);
		mAddEvent->set();
		mCSList->leave();
	}

	/**
	 * Removes first item in the list
	 */
	void popFront()
	{
		mCSList->enter();
		mInternal.pop_front();
		mCSList->leave();
	}

	/**
	 * Returns the number of items stored in the list.
	 * @return Number of items in the list
	 */
	size_t size() const
	{
		mCSList->enter();
		const size_t res = mInternal.size();
		mCSList->leave();
		return res;
	}

	/**
	 * Returns true if the list is empty
	 * @return True if there are no items in the list, false otherwise
	 */
	bool empty() const
	{
		mCSList->enter();
		const bool res = mInternal.empty();
		mCSList->leave();
		return res;
	}

	/**
	 * Closes the list. After closing, it is possible to reach the end of the list using iterators.
	 */
	void close()
	{
		mCSList->enter();
		mClosed = true;
		mClosedEvent->set();
		mCSList->leave();
	}

	/**
	 * Opens the list (opposite to close()). After opening, it's impossible to reach the end of the list using iterators.
	 */
	void open()
	{
		mCSList->enter();
		mClosed = false;
		mClosedEvent->reset();
		mCSList->leave();
	}

	/**
	 * Clears the list.
	 */
	void clear()
	{
		mCSList->enter();
		mInternal.clear();
		mCSList->leave();
	}

	iterator begin() 
	{ 
		mCSList->enter();
		if (!mClosed && mInternal.empty())  {
			mCSList->leave();
			CWaitable::waitMultiple(W_INF, 2, false, mAddEvent, mClosedEvent);
			mCSList->enter();
			assert(!mInternal.empty() || mClosed);
			mAddEvent->reset();
			mClosedEvent->reset();
		}
		iterator res(this, mInternal.begin()); 
		mCSList->leave();
		return res;
	}


	iterator end()  
	{ 
		mCSList->enter();
		iterator res(this, mInternal.end()); 
		mCSList->leave();
		return res;
	}

	bool isClosed() const 
	{ 
		mCSList->enter();
		const bool res = mClosed;
		mCSList->leave();
		return res; 
	}

	/**
	 * A short-hand for open() and close(), does the same thing
	 */
	void setClosed(bool c)  
	{ 
		mCSList->enter();
		mClosed = c;
		if (c)
			mClosedEvent->set();
		else
			mClosedEvent->reset();
		mCSList->leave();
	}
};


#endif // CPARTIALLIST_H
