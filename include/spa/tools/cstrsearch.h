/*
 cstr extension implemetenting some string search algorithms
 */

#ifndef CSTRSEARCH_H
#define CSTRSEARCH_H

#include "cstr.h"

#define CSTRSEARCH_NOT_SO_NAIVE 0
#define CSTRSEARCH_APOSTOLICO_CROCHEMORE 1
#define CSTRSEARCH_BOYER_MOORE_TURBO 2

class cstrsearch: public cstr {
protected:

	int CSTRCALL AXAMAC(const char *x, unsigned int m, const char *y, unsigned int n, unsigned int* matchedIndices, unsigned int maxMatchesCount) const;
	void CSTRCALL preKmp(const char *x, unsigned int m, int kmpNext[]) const;

	int CSTRCALL TBM(const unsigned char *x, unsigned int m, const unsigned char *y, unsigned int n, unsigned int* matchedIndices, unsigned int maxMatchesCount) const;
	void CSTRCALL preBmGs(const unsigned char *x, unsigned int m, int bmGs[]) const;
	void CSTRCALL suffixes(const unsigned char *x, unsigned int m, int *suff) const;
	void CSTRCALL preBmBc(const unsigned char *x, unsigned int m, int bmBc[]) const;

	int CSTRCALL NSN(const char *x, unsigned int m, const char *y, unsigned int n, unsigned int* matchedIndices, unsigned int maxMatchesCount) const;
	int CSTRCALL naive(const char *x, unsigned int m, const char *y, unsigned int n, unsigned int* matchedIndices, unsigned int maxMatchesCount) const;

	//bool patternFound(int patternIndex, int* matchedIndices, int matchesCount, int maxMatchesCount) const;

	int CSTRCALL find(const char* pattern, unsigned int length, unsigned int startChar, int searchAlgorithm) const;
	int CSTRCALL search(const char* pattern, unsigned int length, unsigned int* matchedIndices, unsigned int maxOccurrences, unsigned int startChar, int searchAlgorithm) const;

	cstr CSTRCALL replace(unsigned int* matchedIndices, unsigned int numIndices, unsigned int patternLength, const char* text, unsigned int textLength);
	cstr CSTRCALL replace(const char* pattern, unsigned int patternLength, const char* text, unsigned int textLength, int searchAlgorithm);
	cstr CSTRCALL replaceOnce(const char* pattern, unsigned int patternLength, const char* text, unsigned int textLength, unsigned int startChar, int searchAlgorithm);
public:
	/* Implicit constructor */
	CSTRCALL cstrsearch() : cstr() { }
	/* Creates new instance from cstr */
	CSTRCALL cstrsearch(const cstr& str) : cstr(str) { }
	/* Copy constructor */
 	CSTRCALL cstrsearch(const cstrsearch& str) : cstr(*(cstr*)&str) { }

	/* Assignement operators from cstr */
	using cstr::operator =;

	/* Typecast operator to cstr */
	operator cstr() {
		return *(cstr*) this;
	}

	// returns index of character where found pattern starts or -1 if not found
	int CSTRCALL find(const char* pattern, unsigned int startChar = 0,
			int searchAlgorithm = CSTRSEARCH_APOSTOLICO_CROCHEMORE) const;
	int CSTRCALL find(const cstr &pattern, unsigned int startChar = 0,
			int searchAlgorithm = CSTRSEARCH_APOSTOLICO_CROCHEMORE) const;

	// returns number of occurrences of the searched pattern
	int CSTRCALL search(const char* pattern, unsigned int* matchedIndices, unsigned int maxOccurrences, unsigned int startChar = 0,
			int searchAlgorithm = CSTRSEARCH_APOSTOLICO_CROCHEMORE) const;
	int CSTRCALL search(const cstr &pattern, unsigned int* matchedIndices, unsigned int maxOccurrences, unsigned int startChar = 0,
			int searchAlgorithm = CSTRSEARCH_APOSTOLICO_CROCHEMORE) const;


	// replaces all occurrences of the searched pattern with given string, returns number of occurrences
	cstr CSTRCALL replace(const char* pattern, const char* text, int searchAlgorithm = CSTRSEARCH_APOSTOLICO_CROCHEMORE);
	cstr CSTRCALL replace(const cstr& pattern, const cstr& text, int searchAlgorithm = CSTRSEARCH_APOSTOLICO_CROCHEMORE);

	// replaces first occurrence of the searched pattern with given string, returns number of occurrences
	cstr CSTRCALL replaceOnce(const char* pattern, const char* text, unsigned int startChar = 0,
			int searchAlgorithm = CSTRSEARCH_APOSTOLICO_CROCHEMORE);
	cstr CSTRCALL replaceOnce(const cstr& pattern, const cstr& text, unsigned int startChar = 0,
			int searchAlgorithm = CSTRSEARCH_APOSTOLICO_CROCHEMORE);
};

#endif
