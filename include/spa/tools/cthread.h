

#ifndef CTHREAD_H
#define CTHREAD_H

#include <windows.h>
#include "..\base\spabase.h"

class CThread  {
protected:
	volatile bool mBreakThread;
	HANDLE mThread;

protected:
	friend DWORD WINAPI threadProc(void *_this);
	virtual int run() = 0;

public:
	SPACALL CThread();
	virtual SPACALL ~CThread();


	virtual bool SPACALL start();
	virtual void SPACALL suspend();
	virtual void SPACALL resume();
	virtual int SPACALL terminate(unsigned int timeoutms);
	virtual void SPACALL kill();

	HANDLE SPACALL getHandle() const { return mThread; }


};

#endif // CTHREAD_H
