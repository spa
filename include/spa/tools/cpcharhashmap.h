/*
 Simple Plugin Architecture
 aut: jv
 est: 060317

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef CPCHARHASHMAP_H
#define CPCHARHASHMAP_H

#include <hash_map>
#include <string.h>

/*
 #ifdef _MSC_VER
 */

#ifdef _STLPORT_VERSION
using namespace std;

class CPCharHashMap_hash_compare {
public:
	bool operator () (const char *a, const char *b) const  { return !strcmp(a, b); }
};

template<class T> class CPCharHashMap: public hash_map<const char *, T, hash<const char *>, CPCharHashMap_hash_compare> {
};

#else

using namespace stdext;


class CPCharHashMap_hash_compare {
public:
	enum {
		bucket_size = 4, min_buckets = 8
	};
	CPCharHashMap_hash_compare() {
	}
	size_t operator( )(const char *s) const {
		register unsigned long h = 5381;
		if (s == NULL) return size_t(h);
		register char c;
		for (; c = *s; ++s)
			h += (h << 5) + c;
		return size_t(h);
	}
	bool operator( )(const char* s1, const char* s2) const {
		return strcmp(s1, s2) < 0;
	}
};

template<class T> class CPCharHashMap: public hash_map<const char*, T, CPCharHashMap_hash_compare> {
};

#endif

/*
 #else



 using namespace std;

 struct CPCharHashMap_hash{
 size_t operator()(const char* s) const{
 unsigned long h = 0;
 for (; *s; ++s) h = 5*h+*s;
 return size_t(h);
 }
 };
 struct CPCharHashMap_eqstr{
 bool operator()(const char* s1, const char* s2) const{
 return strcmp(s1, s2) == 0;
 }
 };


 template<class T> class CPCharHashMap:public hash_map<const char*, T, CPCharHashMap_hash, CPCharHashMap_eqstr>{
 };



 #endif
 */

#endif
