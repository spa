/*
 Simple Plugin Architecture
 aut: jv
 est: 060814

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.


 Argument macros to simplify cmd implementation. Each advances cmd to the next argument.
 These macros print error and return from cmd in case argument is NULL or invalid. They expect char *funid to contain function name,
 to simplify this declaration FUNID(componentName, functionName) macro can be used.

 !!! IMPORTANT !!! 
     These macros return on error. Release all resources, leave all critical sections and stuff like that before using them
 !!! IMPORTANT !!! 

 SPAARG_CSTR(arg)
 SPAARG_DOUBLE(arg)
 SPAARG_FLOAT(arg)
 SPAARG_HEX(arg) -> int arg;
 SPAARG_INT(arg)
 SPAARG_PCHAR(arg) -> char *arg;     Do not delete arg!
 SPAARG_UINT(arg)
 SPAARG_IFACE(arg, interface) -> interface *arg; char *argComponentName;	Use SRELEASE_COMP(arg) to release arg. Do not delete argComponentName!



 These do not return with error in case the argument is missing or invalid. They use a default value instead.
 SPAARG_DCSTR(arg, def)
 SPAARG_DDOUBLE(arg, def)
 SPAARG_DFLOAT(arg, def)
 SPAARG_DHEX(arg, def)
 SPAARG_DINT(arg, def)
 SPAARG_DPCHAR(arg, def)
 SPAARG_DUINT(arg, def)
 */

#ifndef SPAARG_H
#define SPAARG_H

/* macros to define funid variable*/
#define FUNID(clsName, func) char *funid = #clsName"::"#func": ";
#define FUNID_MSG(appendMsg) ((cstr(funid) + appendMsg).c_str())

/* error messages */
extern char *spaargMissing, *spaargBadFormat1, *spaargBadFormat2, *spaargBadFormat3;

/* macro to determine argument existence */

#define SPAARG_MISSING (cmd == NULL || cmd->token == NULL)

/* 
 argument macros with error messages
 !!! IMPORTANT !!! 
     These macros return on error. Release all resources, leave all critical sections and stuff like that before using them
 !!! IMPORTANT !!! 
 */

#define SPAARG_PCHAR(arg) \
if (cmd == NULL || cmd->token == NULL) { \
	logError((cstr(funid) + spaargMissing+#arg + ".").c_str()); \
	return SERR; \
} \
char *arg = cmd->token; \
cmd = cmd->next;

#define SPAARG_CSTR(arg) \
if (cmd == NULL || cmd->token == NULL) { \
	logError((cstr(funid) + spaargMissing+#arg + ".").c_str()); \
	return SERR; \
} \
cstr arg = cmd->token; \
cmd = cmd->next;

#define SPAARG_INT(arg) \
if (cmd == NULL || cmd->token == NULL) { \
	logError((cstr(funid) + spaargMissing+#arg + ".").c_str()); \
	return SERR; \
} \
int arg; \
if (cstr(cmd->token).scanf("%i", &arg) != 1) { \
	logError((cstr(funid) + spaargBadFormat1 + cmd->token + spaargBadFormat2+#arg + spaargBadFormat3 + "an integer.").c_str()); \
	return SERR; \
} \
cmd = cmd->next;

#define SPAARG_UINT(arg) \
if (cmd == NULL || cmd->token == NULL) { \
	logError((cstr(funid) + spaargMissing+#arg + ".").c_str()); \
	return SERR; \
} \
unsigned int arg; \
if (cstr(cmd->token).scanf("%u", &arg) != 1) { \
	logError((cstr(funid) + spaargBadFormat1 + cmd->token + spaargBadFormat2+#arg + spaargBadFormat3 + "an unsigned integer.").c_str()); \
	return SERR; \
} \
cmd = cmd->next;

#define SPAARG_HEX(arg) \
if (cmd == NULL || cmd->token == NULL) { \
	logError((cstr(funid) + spaargMissing+#arg + ".").c_str()); \
	return SERR; \
} \
int arg; \
if (cstr(cmd->token).scanf("%x", &arg) != 1) { \
	logError((cstr(funid) + spaargBadFormat1 + cmd->token + spaargBadFormat2+#arg + spaargBadFormat3 + "hexadecimal.").c_str()); \
	return SERR; \
} \
cmd = cmd->next;

#define SPAARG_FLOAT(arg) \
if (cmd == NULL || cmd->token == NULL) { \
	logError((cstr(funid) + spaargMissing+#arg + ".").c_str()); \
	return SERR; \
} \
float arg; \
if (cstr(cmd->token).scanf("%f", &arg) != 1) { \
	logError((cstr(funid) + spaargBadFormat1 + cmd->token + spaargBadFormat2+#arg + spaargBadFormat3 + "a float.").c_str()); \
	return SERR; \
} \
cmd = cmd->next;

#define SPAARG_DOUBLE(arg) \
if (cmd == NULL || cmd->token == NULL) { \
	logError((cstr(funid) + spaargMissing+#arg + ".").c_str()); \
	return SERR; \
} \
double arg; \
if (cstr(cmd->token).scanf("%lf", &arg) != 1) { \
	logError((cstr(funid) + spaargBadFormat1 + cmd->token + spaargBadFormat2+#arg + spaargBadFormat3 + "a float.").c_str()); \
	return SERR; \
} \
cmd = cmd->next;



#define SPAARG_IFACE(arg, interface) \
if (cmd == NULL || cmd->token == NULL) { \
	logError(FUNID_MSG(spaargMissing + #arg + ".")); \
	return SERR; \
} \
interface *arg = NULL; \
if (mMan->getComponentI(cmd->token, IID_##interface, (void**)&arg) != S_OK || arg == NULL) { \
	logError(FUNID_MSG("Component \"" + cmd->token + "\" not found or does not implement "#interface" with IID_"#interface" = " + cstrguid(IID_##interface) + ".")); \
	return SERR; \
} \
char *arg##ComponentName = cmd->token; \
cmd = cmd->next;



#define SRELEASE_COMP(arg) \
if (arg != NULL) mMan->releaseComponent((IUnknown**)&arg);
 


//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================


/* argument macros with default values */

#define SPAARG_DPCHAR(arg, def) \
char *arg; \
if (cmd != NULL&&cmd->token != NULL) { \
	arg = cmd->token; \
	cmd = cmd->next; \
} else arg = def;

#define SPAARG_DCSTR(arg, def) \
cstr arg; \
if (cmd != NULL&&cmd->token != NULL) { \
	arg = cmd->token; \
	cmd = cmd->next; \
} else arg = def;

#define SPAARG_DINT(arg, def) \
int arg; \
if (cmd != NULL&&cmd->token != NULL) { \
	if (cstr(cmd->token).scanf("%i", &arg) != 1) arg = def; \
	cmd = cmd->next; \
} else arg = def;

#define SPAARG_DUINT(arg, def) \
unsigned int arg; \
if (cmd != NULL&&cmd->token != NULL) { \
	if (cstr(cmd->token).scanf("%u", &arg) != 1) arg = def; \
	cmd = cmd->next; \
} else arg = def;

#define SPAARG_DHEX(arg, def) \
int arg; \
if (cmd != NULL&&cmd->token != NULL) { \
	if (cstr(cmd->token).scanf("%x", &arg) != 1) arg = def; \
	cmd = cmd->next; \
} else arg = def;

#define SPAARG_DFLOAT(arg, def) \
float arg; \
if (cmd != NULL&&cmd->token != NULL) { \
	if (cstr(cmd->token).scanf("%f", &arg) != 1) arg = def; \
	cmd = cmd->next; \
} else arg = def;

#define SPAARG_DDOUBLE(arg, def) \
double arg; \
if (cmd != NULL&&cmd->token != NULL) { \
	if (cstr(cmd->token).scanf("%lf", &arg) != 1) arg = def; \
	cmd = cmd->next; \
} else arg = def;

#endif

