/*
 Simple Plugin Architecture
 aut: jv
 est: 080410

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.


 Property-like macros to automate variable script getters and setters.
 The following prerequisities must be met for property access macros to work:
 1) class variable CCrisec *mCSData must be declared and initialized
 
 The class macro argument is the property owner class identifier - class within which the property is declared.

 The following macros are available:

 interface* property macros to declare, register, access and release a SPA component interface:
 SPAPROP_IFACE_DECL      (class, interface, property)
 SPAPROP_IFACE_REG       (interface, property, cmdstring)
 SPAPROP_IFACE_DEF_BEGIN (class, property)
 SPAPROP_IFACE_DEF_SET   (interface, property)
 SPAPROP_IFACE_DEF_END
 SPAPROP_IFACE_DEF       (class, interface, property)
 SPAPROP_IFACE_RELEASE   (property)

 simple type property macros to declare, register and access variables of integer, floating point and cstr types
 SPAPROP_SIMPLE_DECL      (class, type, property)
 SPAPROP_SIMPLE_REG       (property, defaultValue, cmdstring, description)
 SPAPROP_SIMPLE_DEF_BEGIN (class, argType, property)
 SPAPROP_SIMPLE_DEF_SET   (property)
 SPAPROP_SIMPLE_DEF_END
 SPAPROP_SIMPLE_DEF       (class, argType, property)

 */

#ifndef SPAPROP_H
#define SPAPROP_H

#include "..\tools\cstrguid.h"
#include "spaarg.h"

/* interface* property macros to declare, register, access and release a SPA component interface */

/* macro to declare property - results in "interface *maproperty" variable and "property" command being declared */

#define SPAPROP_IFACE_DECL(class, interface, property) \
interface *property; \
SPACMDDECL(class, property);

/* macro to register property in constructor */

#define SPAPROP_IFACE_REG(interface, property, cmdstring) \
property = NULL; \
SPACMDREG_(property, cmdstring, "Sets/returns property "#cmdstring" (a component that implements interface "#interface"). Usage: \""cmdstring" [component]\"");

/* macros to define property getter/setter command */

#define SPAPROP_IFACE_DEF_BEGIN(class, property) \
SPACMDDEF(class, property){ FUNID(class, property); \
 if (cmd == NULL || cmd->token == NULL) { \
  mCSData->enter(); \
  if (property == NULL){ mCSData->leave(); return NULL;} \
  ISPAComponent *aux = NULL; \
  HRESULT qires = property->QueryInterface(IID_ISPAComponent, (void**)&aux); \
  if (qires != S_OK || aux == NULL) { logError((cstr(funid) + " Cannot query ISPAComponent from "#property".").c_str()); mCSData->leave(); return SERR; } \
  char *res = mMan->newStr(aux->name()); \
  aux->Release(); \
  mCSData->leave(); \
  return res; \
 } \
 char *spapropdefComponentName = cmd->token;

/*
 any code may precede the ...SET... macro including cmd processing (cmd still points to the 1st argument (component name)) or return
 the code will be executed before the property is actually set
 macro declares an int success variable which is 1, if property is successfully set, or 0 otherwise
 */

#define SPAPROP_IFACE_DEF_SET(interface, property) \
 int success = 1; { \
  interface *aux = NULL; \
  if (mMan->getComponentI(spapropdefComponentName, IID_##interface, (void**)&aux) != S_OK || aux == NULL) { \
   logError((cstr(funid) + "Component \"" + spapropdefComponentName + "\" not found or does not implement "#interface" with IID_"#interface" = " + cstrguid(IID_##interface) + ".").c_str()); \
   success = 0; \
  } else { \
   mCSData->enter(); \
   if (property != NULL) mMan->releaseComponent((IUnknown**)&property); \
   property = aux; \
   mCSData->leave(); \
  }\
 }

/*
 any code may follow the ...SET... macro including return, the code will be executed after the property has been attempted to set
 success of the attempt can be determined via int success variable (1 on success, 0 otherwise)
 the code preceeding the ...END... macro must leave all critical sections and release all allocated resources
 */

#define SPAPROP_IFACE_DEF_END \
 if (!success) return SERR; \
 return SOK; \
}

/* all three macros together */

#define SPAPROP_IFACE_DEF(class, interface, property) \
SPAPROP_IFACE_DEF_BEGIN(class, property) \
SPAPROP_IFACE_DEF_SET(interface, property) \
SPAPROP_IFACE_DEF_END

/* macro to release component (for example in finish() method) */
#define SPAPROP_IFACE_RELEASE(property) \
	if (property != NULL) mMan->releaseComponent((IUnknown**)&property); \
	(property) = NULL;

//============================================================================================================================================
//============================================================================================================================================
//============================================================================================================================================
//============================================================================================================================================
//============================================================================================================================================


/* simple type property macros to declare, register and access variables of integer, floating point and cstr types */

/*
 macro to declare property - results in "type *maproperty" variable and "property" command being declared
 integer and floating point types and cstr class can be used as type
 */

#define SPAPROP_SIMPLE_DECL(class, type, property) \
	type property; \
	SPACMDDECL(class, property);

/* macro to register property in constructor */

#define SPAPROP_SIMPLE_REG(property, defaultValue, cmdstring, description) \
property = defaultValue; \
SPACMDREG_(property, cmdstring, "Sets/returns property "#cmdstring" ("description"). Usage: \""cmdstring" [value]\"");

/*
 macros to define property getter/setter command
 use an arg... macro sufix in aargType (INT, UINT, HEX, FLOAT, DOUBLE, CSTR, PCHAR)
 */

#define SPAPROP_SIMPLE_DEF_BEGIN(class, argType, property) \
SPACMDDEF(class, property) { \
 if (cmd == NULL || cmd->token == NULL){ \
  mCSData->enter(); \
  char *res = mMan->newStr(cstr(property).c_str()); \
  mCSData->leave(); \
  return res; \
 } \
 FUNID(class, property); \
 SPAARG_##argType(value);

/*
 any code may precede the ...SET... macro including cmd processing (cmd points to the 2nd argument) or return
 the code will be executed before the property is actually set
 */

#define SPAPROP_SIMPLE_DEF_SET(property) { \
 mCSData->enter(); \
 property = value; \
 mCSData->leave(); \
 }

/*
 any code may follow the ...SET... macro including return, the code will be executed after the property has been set
 the code preceeding the ...END... macro must leave all critical sections and release all allocated resources
 */

#define SPAPROP_SIMPLE_DEF_END \
 return SOK; \
}

/* all three macros together */

#define SPAPROP_SIMPLE_DEF(class, argType, property) \
SPAPROP_SIMPLE_DEF_BEGIN(class, argType, property) \
SPAPROP_SIMPLE_DEF_SET(property) \
SPAPROP_SIMPLE_DEF_END

#endif
