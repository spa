/*
 Simple Plugin Architecture
 aut: jv
 est: 060425

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef CLOG_H
#define CLOG_H

#include "..\base\spabase.h"

class CLog {
protected:
	virtual int SPACALL logInfo(char* msg) = 0;
	virtual int SPACALL logWarn(char* msg) = 0;
	virtual int SPACALL logError(char* msg) = 0;
	virtual int SPACALL logFatal(char* msg) = 0;
	/*virtual int SPACALL mpinfo(char* msg) = 0;
	virtual int SPACALL mpwarn(char* msg) = 0;
	virtual int SPACALL mperror(char* msg) = 0;
	virtual int SPACALL mpfatal(char* msg) = 0;*/
public:
};

#endif
