/*
 Simple Plugin Architecture
 aut: jv
 est: 060426

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef CSPAAUXCOMPONENT_H
#define CSPAAUXCOMPONENT_H

#include "clog.h"
#include "cspaauxout.h"

class CSPAAuxComponent: virtual public CLog {
protected:
	class mCAuxOut: public CSPAAuxOut {
	protected:
		CSPAAuxComponent* mOut;
	public:
		virtual int SPACALL logInfo(char* msg) {return mOut->logInfo(msg);}
		virtual int SPACALL logWarn(char* msg) {return mOut->logWarn(msg);}
		virtual int SPACALL logError(char* msg) {return mOut->logError(msg);}
		virtual int SPACALL logFatal(char* msg) {return mOut->logFatal(msg);}
		/*virtual int SPACALL mpinfo(char* msg) {return mOut->mpinfo(msg);}
		virtual int SPACALL mpwarn(char* msg) {return mOut->mpwarn(msg);}
		virtual int SPACALL mperror(char* msg) {return mOut->mperror(msg);}
		virtual int SPACALL mpfatal(char* msg) {return mOut->mpfatal(msg);}*/
		SPACALL mCAuxOut(CSPAAuxComponent* out) {mOut = out;}
	};
	friend class mCAuxOut;
	mCAuxOut* mOut;
public:
	SPACALL CSPAAuxComponent() {
		mOut = new mCAuxOut(this);
	}
	virtual SPACALL ~CSPAAuxComponent() {
		delete mOut;
	}
};

#endif
