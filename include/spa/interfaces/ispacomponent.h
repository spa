/*
 Simple Plugin Architecture
 aut: jv
 est: 060327

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef ISPACOMPONENT_H
#define ISPACOMPONENT_H

/* command */
typedef struct TSPACMD_ {
	char *token;
	TSPACMD_ *next;
} TSPACMD;

#define INITGUID
#define __INLINE_ISEQUAL_GUID
#include <objbase.h>

/*IID format:
 data1: the same for all SPA interfaces,
 data2: SPA interface num.,
 data3: version,
 data4[]: the same for all SPA interfaces
 */
DEFINE_GUID(IID_ISPAComponent, 0x6F15F139, 0x0000, 0x0000, 0xA6, 0x72, 0x41,
		0x2F, 0xD9, 0xE6, 0x90, 0x97);

DECLARE_INTERFACE_(ISPAComponent, IUnknown) {
	//DECLARE_INTERFACE(ISPAComponent) {
	//DECLARE_INTERFACE(ISPAComponent: virtual public IUnknown) {
	STDMETHOD_(char*, call)(TSPACMD* cmd)PURE;
	STDMETHOD_(void, finish)()PURE;
	STDMETHOD_(char*, name)()PURE;
};

#endif
