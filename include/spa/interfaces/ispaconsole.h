/*
 Simple Plugin Architecture
 aut: jv
 est: 060609

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef ISPACONSOLE_H
#define ISPACONSOLE_H

/* message types */
#define SPACONMSG_INFO  0L
#define SPACONMSG_WARN  1L
#define SPACONMSG_ERROR 2L
#define SPACONMSG_FATAL 3L

/* message flags */
#define SPACONMSGFLG_POP   1L

#define INITGUID
#define __INLINE_ISEQUAL_GUID
#include <objbase.h>

/*IID format:
 data1: the same for all SPA interfaces,
 data2: SPA interface num.,
 data3: version,
 data4[]: the same for all SPA interfaces
 */
DEFINE_GUID(IID_ISPAConsole, 0x6F15F139, 0x0002, 0x0000, 0xA6, 0x72, 0x41,
		0x2F, 0xD9, 0xE6, 0x90, 0x97);

DECLARE_INTERFACE_(ISPAConsole, IUnknown) {
	//DECLARE_INTERFACE(ISPAConsole) {
	STDMETHOD_(int, message)(char* name, char* msg, int type, int flags)PURE;
};

#endif
