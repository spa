/*
 Simple Plugin Architecture
 aut: jv
 est: 061103

	This file is part of SPA (Simple Plugin Architecture).

    SPA (Simple Plugin Architecture) is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPA (Simple Plugin Architecture) is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPA (Simple Plugin Architecture).  If not, see <http://www.gnu.org/licenses/>.

 */

#ifndef ISPAMANAGER_H
#define ISPAMANAGER_H

#define INITGUID
#define __INLINE_ISEQUAL_GUID
#include <objbase.h>
#include "ispacomponent.h"

/*IID format:
 data1: the same for all SPA interfaces,
 data2: SPA interface num.,
 data3: version,
 data4[]: the same for all SPA interfaces
 */
DEFINE_GUID(IID_ISPAManager, 0x6F15F139, 0x0001, 0x0001, 0xA6, 0x72, 0x41,
		0x2F, 0xD9, 0xE6, 0x90, 0x97);

DECLARE_INTERFACE_(ISPAManager, IUnknown) {
	//DECLARE_INTERFACE(ISPAManager) {
	STDMETHOD_(void, delStr)(char** str)PURE;
	STDMETHOD_(void, delCmd)(TSPACMD** cmd)PURE;
	STDMETHOD_(char*, execute)(char* cmd)PURE;
	STDMETHOD_(char*, executeCmd)(TSPACMD* cmd)PURE;
	STDMETHOD_(ISPAComponent*, getComponent)(char* name)PURE;
	STDMETHOD(getComponentI)(char* name, REFIID riid, void** ppvObj)PURE;
	STDMETHOD_(char*, newStr)(char* str)PURE;
	STDMETHOD_(TSPACMD*, newToken)(char* str)PURE;
	STDMETHOD_(int, releaseComponent)(IUnknown** component)PURE;
	STDMETHOD_(TSPACMD*, tokenize)(char* cmd)PURE;
};

#endif
